#include "../tools/mytool.h"
#include "../Utils/argvparser.h"
#include "TROOT.h"
#include "TApplication.h"
#include "../Utils/Timer.h"


using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;
using namespace CommandLineProcessing;

INITIALIZE_EASYLOGGINGPP

int main ( int argc, char* argv[] )
{
    //configure the logger
    el::Configurations conf ("settings/logger.conf");
    el::Loggers::reconfigureAllLoggers (conf);

    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription ( "CMS Ph2 DAQ Workshop test application" );
    // error codes
    cmd.addErrorCode ( 0, "Success" );
    cmd.addErrorCode ( 1, "Error" );
    // options
    cmd.setHelpOption ( "h", "help", "Print this help page" );

    cmd.defineOption ( "file", "Hw Description File . Default value: settings/Calibration8CBC.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/ );
    cmd.defineOptionAlternative ( "file", "f" );

    cmd.defineOption ( "batch", "Run the application in batch mode", ArgvParser::NoOptionAttribute );
    cmd.defineOptionAlternative ( "batch", "b" );

    cmd.defineOption("testgroup", " The test group the test shall be run", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative ( "testgroup", "t" );

    cmd.defineOption("channel", " The channel the test shall be run", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative ( "channel", "c" );

    cmd.defineOption("steps", " The steps the test shall be run", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative ( "steps", "s" );

    cmd.defineOption("tmin", " The t0 that the test shall be run", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative ( "tmin", "m" );

    cmd.defineOption("tmax", " The t0 that the test shall be run", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative ( "tmax", "M" );


    //actually parse
    int result = cmd.parse ( argc, argv );

    if ( result != ArgvParser::NoParserError )
    {
        LOG (INFO) << cmd.parseErrorDescription ( result );
        exit ( 1 );
    }

    // now query the parsing results
    std::string cHWFile = ( cmd.foundOption ( "file" ) ) ? cmd.optionValue ( "file" ) : "settings/Calibration8CBC.xml";
    //std::string cDirectory = "Results/mytest";
    bool batchMode = ( cmd.foundOption ( "batch" ) ) ? true : false;

    std::string tg= ( cmd.foundOption ( "testgroup" ) ) ? cmd.optionValue ( "testgroup" ) : "0";
    std::string ch= ( cmd.foundOption ( "channel" ) ) ? cmd.optionValue ( "channel" ) : "0";
    std::string cst= ( cmd.foundOption ( "steps" ) ) ? cmd.optionValue ( "s" ) : "0";
    std::string tmin= ( cmd.foundOption ( "tmin" ) ) ? cmd.optionValue ( "m" ) : "0";
    std::string tmax= ( cmd.foundOption ( "tmax" ) ) ? cmd.optionValue ( "M" ) : "0";


    TApplication cApp ( "Root Application", &argc, argv );

    if ( batchMode ) gROOT->SetBatch ( true );
    else TQObject::Connect ( "TCanvas", "Closed()", "TApplication", &cApp, "Terminate()" );

    Timer t;

    //create a genereic Tool Object, I can then construct all other tools from that using the Inherit() method
    //this tool stays on the stack and lives until main finishes - all other tools will update the HWStructure from cTool
    Tool cTool;
    std::stringstream outp;
    cTool.InitializeHw ( cHWFile, outp );
    cTool.InitializeSettings ( cHWFile, outp );
    LOG (INFO) << outp.str();
    outp.str ("");
    std::string cDirectory = "Results/pulseshape_tg" +tg+"_ch"+ch;
    cTool.CreateResultDirectory ( cDirectory);
   //cTool.CreateResultDirectory ( cDirectory );
    cTool.InitResultFile ( "mytestfile" );
    cTool.ConfigureHw ();
    std::string::size_type sz;
    t.start();
    //HW configured according to config file, everything ready for us to take over!
    //her goes our code
    //the constructor with channel number and test group
    //mytool cMyTool (0, 59);
    //mytool cMyTool (0, 1);
    LOG (INFO)<<"Doing tests on tg" <<tg<< " and ch "<<ch ;
    //mytool cMyTool (6, 75); //std::stoi (str_dec,&sz);
    //mytool cMyTool (std::stoi (tg,&sz),std::stoi (ch, &sz), std::stoi (cst,&sz), std::stoi (tmin,&sz), std::stoi (tmax,&sz));
    mytool cMyTool (std::stoi (ch,&sz),std::stoi (tg, &sz), std::stoi (cst,&sz), std::stoi (tmin,&sz), std::stoi (tmax,&sz));
    cMyTool.Inherit (&cTool);
    cMyTool.Initialize();
    cMyTool.SweepTPDelay();

    t.stop();
    t.show ( "Time to run our code: " );
    
    //clean up and exit cleanly
    cTool.SaveResults();
    cTool.CloseResultFile();
//    cTool.Destroy();

    if ( !batchMode ) cApp.Run();

    return 0;
}
