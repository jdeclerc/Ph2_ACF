from PSDAQ import *
from mpa_methods.mpa_i2c_conf import *

## methods
def FindLatency(npoints = 200):
	# overwrite the number of triggers
	fc7.write("cnfg_fast_triggers_to_accept", npoints)
	# also disable resync
	fc7.write("cnfg_fast_tp_fsm_fast_reset_en", 0)
	# some delays
	fc7.write("cnfg_fast_delay_after_fast_reset", 50)
  	fc7.write("cnfg_fast_delay_after_test_pulse", 200)
  	fc7.write("cnfg_fast_delay_before_next_pulse", 200)
	sleep(0.01)
        fc7.write("ctrl_fast_load_config", 1)
	# activate chip i2c just in case
	activate_I2C_chip()

	latency_min = 196
	latency_range = 1
	latency = 196
	stub_latency = 161
	for latency in range(latency_min,latency_min+latency_range):
	#for stub_latency in range(latency_min,latency_min+latency_range):
		# set the latency
		I2C.row_write("L1Offset_1", 0, (0x00FF & latency) >> 0)
		I2C.row_write("L1Offset_2", 0, (0x0100 & latency) >> 8)
		# stub latency
        	fc7.write("cnfg_readout_common_stubdata_delay", stub_latency)

		# start run
		DAQController.StartRun(runProcessor = False)

		# process data
		npixel_clusters = 0
		nstrip_clusters = 0
		nstubs = 0
		while (DAQController.EventCounter < npoints):
		    if not DAQController.DataQueue.empty():
			RAWData = DAQController.DataQueue.get()
			if RAWData is None:
			    pass
			else:
			    nevents = len(RAWData)/DAQController.EventSize
			    DAQController.EventCounter += nevents
			    print "Got " + str(nevents) + " events package (total " + str(DAQController.EventCounter) + ")"
			    for i in range(0, nevents):
				# create d19c event
				d19cEvent = PSEventVR(RAWData[i*DAQController.EventSize:(i+1)*DAQController.EventSize])
				# store into root file if needed
				if DAQController.SaveROOT:
				    DAQController.FillROOTTree(d19cEvent)
				# process latency
				npixel_clusters += d19cEvent.GetNPixelClusters()
				nstrip_clusters += d19cEvent.GetNStripClusters()
				nstubs += len(d19cEvent.GetStubs())
				#print "test l1 counter: ", d19cEvent.GetMPAL1Counter()
			DAQController.DataQueue.task_done()
		# print the latency
		print "Latency: ", latency, "Stub Latency: ", stub_latency, ", N Pixel Clusters: ", npixel_clusters, ", N Strip Clusters: ", nstrip_clusters, " N Stubs: ", nstubs

		# stop run	
		DAQController.StopRun()

## run program

## settings
# external clock enable
ext_clock_en = 0
# trigger source (1 - ttc, 2 - stubs, 3 - user frequency (1mhz), 4 - tlu, 5 - ext, 6 - cal pulse, 7 - consecutive, 8 - antenna)
trigger_source = 6
# external trigger delay (for both tlu and ext)
ext_trigger_delay = 50
# if disabled has to be treated by the tlu or trigger board
backpressure_en = 1
# no stubs on this beam test
stub_trigger_delay = 200
stub_veto_length = 50
readout_stub_delay = 161
# npackets in the readout package
npackets = 99
# enable handshake
handshake_en = 1
# enable dio5/tlu
dio5_tlu_enable = 0

## start
# init daq controller
DAQController = PSDAQ(SaveROOT = True, EmulatorMode = False)
# configure everything
DAQController.ConfigureAll(ext_clock_en, trigger_source, ext_trigger_delay, backpressure_en, stub_trigger_delay, stub_veto_length, npackets, handshake_en, readout_stub_delay, dio5_tlu_enable)


## run find latency
FindLatency()

