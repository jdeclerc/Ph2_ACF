import csv
import numpy as np
from numpy import pi, r_
import matplotlib.pyplot as plt
from scipy import optimize
from scipy import special
import time
import sys
from scipy.stats import norm
import matplotlib.mlab as mlab

#settings, these are dependent on the input files!!
CHIP_type = "CBC3" #CBC2 or CBC3
chip_mode = "electron" #hole or electron

VCTH_min = 300  #CBC2:100
VCTH_max = 700  #CBC2: 254
VCTH_range = VCTH_max - VCTH_min  
VCTH_step = 1

TP_min = 200
TP_max = 221
TP_step = 10

#to put in the histo's in the end. 
gains_LSB = []
gains_electrons = []
gains_mV = []
noise_all = []
average_noise = []


VCTH_list = np.arange(VCTH_min,VCTH_max,VCTH_step)
TP_list = np.arange(TP_min,TP_max,TP_step)
print(TP_list)
#the function to fit the S - curves
def fitfunc_Scurve(x,pedestal,noise):
	if(chip_mode == "hole"):
		return 0.5+0.5*special.erf((-x-pedestal)/(np.sqrt(2)*noise))
	elif(chip_mode == "electron"):
		return 0.5+0.5*special.erf((x-pedestal)/(np.sqrt(2)*noise))


#def fitfunc_Sigmoid(x,b,c):
	#return a/(1+b*np.exp(x+c))+d
#	return 1/(1+b*np.exp(x+c))

#p0_Sigmoid = [1,-140]

#def inverse_Sigmoid(y,a,b,c,d):
#	return -np.log((a/(y-d)-1)/b)+c


#the function to fit the gains, just a line
def fitfunc_gain(x,gain_offset,gain):
	return gain_offset+gain*x

p0_gain = [120,1.5]


#define the TP groups:
channel_TG_0 = [0,1,16,17,32,33,48,49,64,65,80,81,96,97,112,113,128,129,144,145,160,161,176,177,192,193,208,209,224,225,240,241]
channel_TG_1 = [x+2 for x in channel_TG_0]
channel_TG_2 = [x+2 for x in channel_TG_1]
channel_TG_3 = [x+2 for x in channel_TG_2]
channel_TG_4 = [x+2 for x in channel_TG_3]
channel_TG_5 = [x+2 for x in channel_TG_4]
channel_TG_6 = [x+2 for x in channel_TG_5]
channel_TG_7 = [x+2 for x in channel_TG_6]

print (channel_TG_0)
print (channel_TG_1)
print (channel_TG_2)
print (channel_TG_3)
print (channel_TG_4)
print (channel_TG_5)
print (channel_TG_6)
print (channel_TG_7)

def channel_in_test_pulse_group(test_pulse_group, i_channel):
	test_pulse_group_found = 9
	if i_channel in channel_TG_0:
		test_pulse_group_found = 0
	elif i_channel in channel_TG_1:
		test_pulse_group_found = 1
	elif i_channel in channel_TG_2:
		test_pulse_group_found = 2
	elif i_channel in channel_TG_3:
		test_pulse_group_found = 3
	elif i_channel in channel_TG_4:
		test_pulse_group_found = 4
	elif i_channel in channel_TG_5:
		test_pulse_group_found = 5
	elif i_channel in channel_TG_6:
		test_pulse_group_found = 6
	elif i_channel in channel_TG_7:
		test_pulse_group_found = 7

	if (test_pulse_group == test_pulse_group_found):
		return True
	else:
		return False


#2D array to keep the pedestals, errors on the pedestal, noise and noise on the pedestal for certain TP and certain channel
n_channels = 254
S_pedestals = [[0 for x in range(n_channels)] for y in range(len(TP_list))]
S_pedestals_error = [[0 for x in range(n_channels)] for y in range(len(TP_list))]
S_noise = [[0 for x in range(n_channels)] for y in range(len(TP_list))]
S_noise_error = [[0 for x in range(n_channels)] for y in range(len(TP_list))]

def extract_gains(test_pulse_group, folder_name):
    #start looping over the files with occupancies and VCTHs for different TP amplitudes
    i_test_pulse = 0
    for test_pulse in TP_list:
        #read the correct file from a certain test pulse group and a certain test pulse amplitude
        filename = str("gain_measurement/Results/"+str(folder_name)+"/"+str(test_pulse_group)+"/occupancies_TP_"+str(test_pulse)+".csv")
        file=open( filename, "r")
        reader = csv.reader(file)
        print( "running on file " , filename)
        S_curves_one_TP_ampl = [[0 for x in range(254)] for y in range(VCTH_range)] 

        i_line = 0
        #fill a 2D array of occupancies for certain channels and certain VCTh settings
        for line in reader: #each line corresponds to a certain VCTH setting
            for i_channel in range(0,254):
                occupancy = float(line[i_channel])
                VCTH = i_line
                S_curves_one_TP_ampl[VCTH][i_channel] = float(occupancy)
            i_line = i_line + 1
            #print("going through line #", i_line )

        #now for each channel fit an S-curve and get the pedestal	
        for i_channel in range(0,254):
            occupancies = [row[i_channel] for row in S_curves_one_TP_ampl]
            
            #find a raw estimate for the pedestal

            prev_occupancy = 0
            raw_pedestal_cnt_est = 0
            raw_pedestal_cnt = 0

            for occupancy in occupancies:
                if(prev_occupancy > 0.5 and occupancy < 0.5 and chip_mode == "hole"):
                    raw_pedestal_cnt_est = raw_pedestal_cnt
                if(prev_occupancy < 0.5 and occupancy > 0.5 and chip_mode == "electron"):
                	raw_pedestal_cnt_est = raw_pedestal_cnt
                prev_occupancy = occupancy
                raw_pedestal_cnt = raw_pedestal_cnt + 1

            #p1 = optimize.curve_fit(fitfunc_Sigmoid,VCTH_list,occupancies,[1,-VCTH_list[raw_pedestal_cnt_est]])
            #now do the actual S curve fit
            p1, pcov = optimize.curve_fit(fitfunc_Scurve,VCTH_list,occupancies,[-VCTH_list[raw_pedestal_cnt_est],1] , maxfev=10000)
            if(chip_mode == "electron"):	
            	p1, pcov = optimize.curve_fit(fitfunc_Scurve,VCTH_list,occupancies,[VCTH_list[raw_pedestal_cnt_est],1] , maxfev=10000)
            #get the errors
            perr = np.sqrt(np.diag(pcov))


            S_pedestals[i_test_pulse][i_channel] = -p1[0] 
            if(chip_mode == "electron"):
            	S_pedestals[i_test_pulse][i_channel] = p1[0] 
            noise = p1[1]
            S_noise[i_test_pulse][i_channel] = noise
            noise_all.append(noise)
            
            S_pedestals_error[i_test_pulse][i_channel] = perr[0]
            S_noise_error[i_test_pulse][i_channel] = perr[1]
            S_curve = fitfunc_Scurve(VCTH_list, p1[0], p1[1])
            #plot some example S curves
            #if(i_channel < 10):
            #	plt.plot(VCTH_list,occupancies, 'ro')
            #	plt.plot(VCTH_list,S_curve)
            #	plt.show()

        i_test_pulse = i_test_pulse + 1
        #end of loop over the files
        
    #now fit the S_pedestals matrix for each channel with a linear fit to extract the gain
    for i_channel in range(0,254):
        print( "---------------------------------------------------")
        pedestals = [row[i_channel] for row in S_pedestals]
        print( pedestals)
        pedestals_error = [row[i_channel] for row in S_pedestals_error]
        #fit the gain for the channels in the Test group, just a linear function
        member_TP_group = channel_in_test_pulse_group(test_pulse_group, i_channel)
        print("running on channels ", i_channel, " outcome of in test pulse group check: ", member_TP_group)
        p0_gain_local = p0_gain 
        TP_list_local = TP_list

        #if(not member_TP_group):
        #	p0_gain_local = [120,-1]
        #	TP_list_local = TP_list/40

        p1, pcov = optimize.curve_fit(fitfunc_gain,TP_list_local,pedestals, p0_gain_local,sigma = pedestals_error)
        perr = np.sqrt(np.diag(pcov))
        gain = p1[1]
        gain_error = perr[1]
        gain_electrons = 536.812/gain
        gain_mV = 27.4186*gain

        if (member_TP_group):
        	gains_LSB.append(gain)
        	gains_electrons.append(gain_electrons)
        	gains_mV.append(gain_mV)
        	print("member TG found")

        print("relative error on the gain: ", gain_error/gain)
        output_string =  str("channel, gain_LSB gain_electrons, gain_mV: " + str(i_channel)+ ", " + str(gain) + "," + str(gain_electrons) + "," + str(gain_mV)  + '\n')
        print( output_string)
        gain_fit = fitfunc_gain(TP_list_local, p1[0], p1[1])

        noises = [row[i_channel] for row in S_noise]
        average_noise.append(sum(noises)/len(noises))


      #  pedestal_over_noise = []
      #  for i_TP in range(0, len(TP_list)):
      #      pedestal_over_noise.append(pedestals[i_TP]/noises[i_TP])
      	#plot some example gain fits
        if(member_TP_group):
        #if(i_channel < 20):
	        plt.plot(TP_list_local,pedestals, 'ro')
	        plt.plot(TP_list_local,gain_fit)
	        plt.title("gain for channel"+str(i_channel))
	        plt.show()



for test_pulse_group in range(0,1):
    extract_gains(test_pulse_group, sys.argv[1])
"""    
n, bins, patches = plt.hist(gains_electrons, 200, facecolor='g', alpha=0.75)
plt.xlabel('Gain (e-/VCTH)')
plt.title('Gain')
plt.axis([-1000, 1000,0,30])
plt.grid(True)
plt.show()
"""
print("THE GAINS IN LSB")
print(gains_LSB)

print("THE GAINS IN e/VCTH")
print(gains_electrons)

print("THE GAINS IN MV/fC")
print(gains_mV)


#fit the gain with a gaussian
(mu_gain_mV, sigma_gain_mV) = norm.fit(gains_mV)
fit_gains_mV = mlab.normpdf( gains_mV, mu_gain_mV, sigma_gain_mV)
l = plt.plot(gains_mV, 20*fit_gains_mV, 'ro')

print("output from the gaussian fit to the gains_mV", mu_gain_mV, " ", sigma_gain_mV)
#plt.show()

n2, bins2, patches2 = plt.hist(gains_mV, 400, facecolor='g', alpha=0.75)
plt.xlabel('Gain (mV/fC)')
plt.title('Gain')
plt.axis([0, 200,0,100])
plt.grid(True)
plt.show()
"""
plt.plot(gains_LSB, average_noise, 'ro')
plt.xlabel("gain (LSB)")
plt.ylabel("average noise (LSB)")
plt.show()

#print(noise_all)
n3, bins3, patches3 = plt.hist(noise_all, 100, facecolor='g', alpha=0.75)
plt.xlabel('Noise (VCTH)')
plt.title('Noise')
plt.axis([0, 10,0,100])
plt.grid(True)
plt.show()
"""
