from fc7_daq_methods import *

n_triggers = 1000

#setup the chip
CBC_ConfigTXT('FE0CBC0.txt',0)#'Cbc2_default_hole.txt' or 2CBC2_hybrid_calibrated_CBC0.txt 
CBC_ConfigTXT('FE0CBC1.txt',1)
sleep(0.1)

SendCommand_I2C(            2      ,   0,       1,    0,     0,               0b00001111,    0b11100000,        0) #set the test pulse on
SendCommand_I2C(            2      ,   0,       1,    0,     0,               0b00001101,    0xfe,        0) #set test pulse potentiometer
sleep(0.1)


print "START SENDING THE TEST PULSES"
for i_test_pulse_group in range(0,8):
	print("setting test pulse group: ", i_test_pulse_group)
	SendCommand_I2C(            2      ,   0,       1,    0,     0,               0b00001110,    i_test_pulse_group,        0)	
	i_trigger = 0
	for i_trigger in range (0, n_triggers):
		SendCommand_CTRL("fast_test_pulse")
		sleep(0.0001)
		i_trigger=i_trigger+1
	i_test_pulse_group = i_test_pulse_group + 1
