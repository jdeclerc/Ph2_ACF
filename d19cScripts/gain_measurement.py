import sys
#sys.path.append('../')
from fc7_daq_methods import *
import os

TP_ampl_min = 200
TP_ampl_max = 250
TP_ampl_step = 10

#VCTH_min = 100
#VCTH_max = 254
VCTH_step = 1

VCTH_min = 400
VCTH_max = 700

test_pulse_on = True

################
## fast config #
################
# trigger_source: 1 - L1, 2 - Stubs Coincidence, 3 - User Frequency, 4 - TLU, 5 - External from DIO5, 6 - triggers from test pulse sending machine
trigger_source = 3
if test_pulse_on == True:
	trigger_source = 6
# triggers_to_accept: 0 - continious triggering, otherwise sends neeeded amount and turns off
triggers_to_accept = 100
# trigger_user_frequency: in kHz 1 to 1000.
trigger_user_frequency = 1
# trigger_stubs_mask: can set a stubs coincidence, 5 means that stubs from hybrids id=2 and id=0 are required: 1b'101
trigger_stubs_mask = 5
# stub trigger latency
trigger_stub_delay = 195
#so called stub latency
trigger_stub_latency = 40


chip_type = "CBC3_real"
header_size = 6
n_chips = 2
if(chip_type == "SSA_emulator"):
	payload_size = 7	
elif(chip_type == "CBC_emulator"):
	payload_size = 11
elif(chip_type == "CBC2_real" or chip_type == "CBC3_real"):
	payload_size = 11

def reverse_mask(x):
    x = ((x & 0x55555555) << 1) | ((x & 0xAAAAAAAA) >> 1)
    x = ((x & 0x33333333) << 2) | ((x & 0xCCCCCCCC) >> 2)
    x = ((x & 0x0F0F0F0F) << 4) | ((x & 0xF0F0F0F0) >> 4)
    x = ((x & 0x00FF00FF) << 8) | ((x & 0xFF00FF00) >> 8)
    x = ((x & 0x0000FFFF) << 16) | ((x & 0xFFFF0000) >> 16)
    return x

def Set_TestPulseDelayAndChGroup_CBC2_3(TestPulseDelay,TestPulseGroup):
	TestPulseDelayRevBit = reverse_mask(TestPulseDelay) >> 27
	TestPulseGroupRevBit = reverse_mask(TestPulseGroup) >> 29
	TestPulseDelayAndChGroup = (TestPulseDelayRevBit << 3) | TestPulseGroupRevBit
	return TestPulseDelayAndChGroup

def phase_tuning_CBC3():
	SendCommand_CTRL("fast_fast_reset")	
	#SendCommand_I2C(            2      ,   0,       0,    0,     0,               18,    0x20,        0)#enable the HIP suppresed output on the stub data by changing register 18
	#SendCommand_I2C(            2      ,   0,       0,    0,     0,               12,    0x14,        0)
	write_VCTH(0x3ff)			
	sleep(0.1)
	fc7.write("ctrl_phy_phase_tune_again", 1)
	count_waiting = 0
	while(fc7.read("stat_phy_phase_tuning_done") == 0):
		sleep(0.5)
		print "Phase tuning in state: ", fc7.read("stat_phy_phase_fsm_state_chip0")
		print "Phase tuning in state: ", fc7.read("stat_phy_phase_fsm_state_chip1")
		print "Waiting for the phase tuning"
		if(count_waiting%10 ==0):
			fc7.write("ctrl_phy_phase_tune_again", 1)
			print("resend phase tuning signal")
		count_waiting = count_waiting+1
					

def write_VCTH(VCTH):
	#setting VCTH value
	if(chip_type == "CBC2_real"):
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               12,    VCTH,        0)
	elif(chip_type == "CBC3_real"):
		VCTH_low = VCTH & 0b0011111111
		VCTH_high = (VCTH & 0b1100000000) >> 8
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               79,    VCTH_low,        0)
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               80,    VCTH_high,        0)
	sleep(0.1)

def set_output_file_S_curve(output_folder,TP_delay, TP_group, TP_amplitude, chip_nr):
	#making new dir to save the data
	directory = str("gain_measurement/Results/"+output_folder+"_delay"+str(TP_delay)+"chip_"+str(chip_nr)+"/"+str(TP_group)+"/")
	if not os.path.exists(directory):
    		os.makedirs(directory)
	name_output_file = str(directory+"occupancies_TP_"+str(TP_amplitude)+".csv")
	return name_output_file

def return_i_channel(i_word, i_letter):
		
	if(i_word == 0):
		i_channel = 190+2*i_letter
	elif(i_word == 1):
		i_channel = 126+2*i_letter
	elif(i_word == 2):
		i_channel = 62+2*i_letter
	elif(i_word == 3):
		i_channel = 0+2*i_letter
	elif(i_word == 4):
		i_channel = 191+2*i_letter
	elif(i_word == 5):
		i_channel = 127+2*i_letter
	elif(i_word == 6):
		i_channel = 63+2*i_letter
	elif(i_word == 7):
		i_channel = 1+2*i_letter
	
        return i_channel 

def write_occupancies_to_file(hits_CBC, output_file):
	hits_CBC[:] = [x/float(triggers_to_accept) for x in hits_CBC]
	print hits_CBC
	print "****"
	#print VCTH, " ", hits_CBC0[1]
	for occupancy in hits_CBC:
		output_file.write(str(occupancy))
		output_file.write(",")
	output_file.write("\n")


def S_curve(TP_group, output_folder, TP_delay, TP_amplitude):
	
	name_output_file_chip0 = set_output_file_S_curve(output_folder, TP_delay, TP_group, TP_amplitude, 0) 
	output_file_chip0  = open(name_output_file_chip0,"w")

	name_output_file_chip1 = set_output_file_S_curve(output_folder, TP_delay, TP_group, TP_amplitude, 1) 
	output_file_chip1  = open(name_output_file_chip1,"w")

	#making the S curve
	for VCTH in range (VCTH_min,VCTH_max,VCTH_step):
		#setting VCTH value
		write_VCTH(VCTH)
		sleep(0.1)
		
		SendCommand_CTRL("start_trigger") 
		i_triggers = 0
		hits_CBC = [0]*256
		hits_CBC0 = [0]*256
		hits_CBC1 = [0]*256
		n_waiting_for_data = 0

		while((triggers_to_accept>0 and i_triggers<triggers_to_accept) or (triggers_to_accept==0)):

			#sometimes taking data fails, restart in that case the sending of the triggers
			if(n_waiting_for_data > 1000):
				print "RESETTING THE READOUT"
				i_triggers = 0
				hits_CBC = [0]*256
				n_waiting_for_data = 0
				setup_fc7(False)	
				sleep(0.1)
				SendCommand_CTRL("start_trigger") 
				sleep(0.1)

			else:	

				if (fc7.read("words_cnt") >= n_words):
					REC_DATA = fc7.fifoRead("ctrl_readout_run_fifo",n_words)
					#print uInt32HexListStr(REC_DATA)
					#print REC_DATA
					for i_word in range (0,8):

						word_CBC0 = REC_DATA[6+i_word]
						word_CBC1 = REC_DATA[17+i_word]
						for i_letter in range(0,32):

							if((i_word == 3 and i_word ==31) or (i_word == 7 and i_word ==31)): #special parts in the data package which do not correspond to any channel
								continue

							i_channel = return_i_channel(i_word, i_letter)
							word_masked_CBC0 = word_CBC0 & ((1 << i_letter)| 0x00000000)
							word_masked_shifted_CBC0 = word_masked_CBC0 >>(i_letter)

							word_masked_CBC1 = word_CBC1 & ((1 << i_letter)| 0x00000000)
							word_masked_shifted_CBC1 = word_masked_CBC1 >>(i_letter)
							#hits_CBC[i_channel] = ((REC_DATA[6+i_word]) & ((1 << i_letter)| 0x00000000) >>(i_letter-1)) + hits_CBC[i_channel]
							hits_CBC0[i_channel] = hits_CBC0[i_channel] + word_masked_shifted_CBC0
							hits_CBC1[i_channel] = hits_CBC1[i_channel] + word_masked_shifted_CBC1
					i_triggers=i_triggers+1

				else:

					sleep(0.001)
					n_waiting_for_data = n_waiting_for_data + 1	
			
		print VCTH
		write_occupancies_to_file(hits_CBC0,output_file_chip0)
		write_occupancies_to_file(hits_CBC1,output_file_chip1)
	output_file_chip0.close()
	output_file_chip1.close()
	SendCommand_I2C(            2      ,   0,       0,    0,     1,               13,    0,        0) 	
	SendCommand_I2C(            2      ,   0,       0,    0,     1,               14,    0,        0) 	
	sleep(0.1)
	print "check test pulse amplitude, delay and group"
	ReadChipData(0,0)

def setup_chip(TestPulseGroup,TestPulseDelay):
	################
	##CHIP config###
	################

	if(chip_type == "SSA_emulator"):
		#SendCommand_I2C(command type, hybrid_id, chip_id, page, read, register_address, data, ReadBack)
		# command type: 0 - write to certain chip, hybrid; 1 - write to all chips on hybrid; 2 - write to all chips/hybrids (same for READ)
		#write to register 1 0xff as data --> this will put a 1 on the last 8 channels which go out
		SendCommand_I2C(            2,         0,       0,    0,     0,               1,    0xff,       0)
		#write to register 16 0xaa as data --> this will put a 0xaa on the last 8 bits of the HIP data which goes out
		SendCommand_I2C(            2,         0,       0,    0,     0,               16,    0xaa,       0)
		#write to register 33 1 as data --> this will put different data on the SSA centroid lines
		SendCommand_I2C(            2,         0,       0,    0,     0,               33,    1,       0)
		sleep(0.5)

	elif(chip_type == "CBC_emulator"):
		print "Doing nothing to configure the CBC emulator"	

	elif(chip_type == "CBC2_real"):
		CBC_ConfigTXT('FE0CBC0.txt',0)#'Cbc2_default_hole.txt' or 2CBC2_hybrid_calibrated_CBC0.txt 
		CBC_ConfigTXT('FE0CBC1.txt',1)#'Cbc2_default_hole.txt' or 2CBC2_hybrid_calibrated_CBC1.txt

		################
		#configure default settings#####
		################

		SendCommand_I2C(            2      ,   0,       0,    0,     0,               3,    0xF0,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               4,    0x14,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               5,    0x20,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               6,    0x1E,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               7,    0x2D,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               8,    0x00,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               9,    0x1E,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               10,    0x4B,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               16,    0x64,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               17,    0x41,        0) 

		################
		#configure non default settings#####
		################

		SendCommand_I2C(            2      ,   0,       0,    0,     0,               0,    0x00,        0) #register 0: 0x00: no hysteresis, 0x3c: full hysteresis
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               2,    0xE7,        0) #register2: hitdetectSLVS, A7 (single); E7(variable)
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               1,     198,        0) #register 1: TriggerLatency	
		
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               15,   0xA0,        0) #register 15: MiscTestPulseCtrl&AnalogMux, set everything as below except for enbabling the TP (enable positive going test pulse)	
		if test_pulse_on == True:
			TestPulseDelayAndChGroup = Set_TestPulseDelayAndChGroup_CBC2_3(TestPulseDelay,TestPulseGroup)	
			print "TestPulseDelayAndChGroup ", bin(TestPulseDelayAndChGroup).zfill(8)
			
			SendCommand_I2C(            2      ,   0,       0,    0,     0,               15,    0xE0,        0) #register 15: MiscTestPulseCtrl&AnalogMux, 0x70 (enable positive going test pulse) 
			SendCommand_I2C(            2      ,   0,       0,    0,     0,               14,    TestPulseDelayAndChGroup,        0)        

	elif(chip_type == "CBC3_real"):
		CBC_ConfigTXT('CBC3_FE0CBC0.txt',0) 
		CBC_ConfigTXT('CBC3_FE0CBC1.txt',1)

		SendCommand_I2C(            2      ,   0,       0,    0,     0,               0,    0x7c,        0) #register 0: 0x00: no hysteresis, 0x3c: full hysteresis
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               1,     199,        0) #register 1: TriggerLatency	
		
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               15,   0xA0,        0) #register 15: MiscTestPulseCtrl&AnalogMux, set everything as below except for enbabling the TP (enable positive going test pulse)	
		if test_pulse_on == True:
			TestPulseDelayAndChGroup = Set_TestPulseDelayAndChGroup_CBC2_3(TestPulseDelay,TestPulseGroup)
			print "TestPulseDelayAndChGroup ", bin(TestPulseDelayAndChGroup).zfill(8)
			
			SendCommand_I2C(            2      ,   0,       0,    0,     0,               15,    0x60,        0) #register 15: MiscTestPulseCtrl&AnalogMux, 0x60 (enable negative going test pulse) 
			SendCommand_I2C(            2      ,   0,       0,    0,     0,               14,    TestPulseDelayAndChGroup,        0)        

		################
		#configure default settings#####
		################
"""
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               3,    0xF0,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               4,    0x14,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               5,    0x20,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               6,    0x1E,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               7,    0x2D,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               8,    0x00,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               9,    0x1E,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               10,    0x4B,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               16,    0x64,        0) 
		SendCommand_I2C(            2      ,   0,       0,    0,     0,               17,    0x41,        0) 
"""


################
##config fast###
################
def setup_fc7(do_phase_tuning):

	SendCommand_CTRL("global_reset")
	sleep(0.1)
	fc7.write("cnfg_fast_delay_after_fast_reset" , 50)
	fc7.write("cnfg_fast_delay_after_test_pulse" , 200)
	fc7.write("cnfg_fast_delay_before_next_pulse", 400)
	Configure_Fast(triggers_to_accept, trigger_user_frequency, trigger_source, trigger_stubs_mask, trigger_stub_latency)
	sleep(0.1)

	#start of phase tuning for CBC3
	if(do_phase_tuning):
		phase_tuning_CBC3()

##start the routine for making a gain measurement

n_words =header_size+n_chips*payload_size

setup_fc7(False)

for TestPulseDelay in xrange(0,25,1):

	for TestPulseGroup in xrange(0,8,1):
		setup_chip(TestPulseGroup,TestPulseDelay)

		for TP_ampl in xrange(TP_ampl_min,TP_ampl_max, TP_ampl_step):
			print("***********************************************************")
			print "Running for TestPulseDelay ", TestPulseDelay, " , TestPulseGroup ", str(TestPulseGroup) , " and TP_ampl ", str(TP_ampl) 
			SendCommand_I2C(            2      ,   0,       0,    0,     0,               13,    TP_ampl,        0)
			S_curve( TestPulseGroup, sys.argv[1], TestPulseDelay, TP_ampl)
#S_curve(int(sys.argv[1]),TestPulseGroup)


