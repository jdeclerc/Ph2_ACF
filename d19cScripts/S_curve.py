from fc7_daq_methods import *
import sys

def reverse_mask(x):
    x = ((x & 0x55555555) << 1) | ((x & 0xAAAAAAAA) >> 1)
    x = ((x & 0x33333333) << 2) | ((x & 0xCCCCCCCC) >> 2)
    x = ((x & 0x0F0F0F0F) << 4) | ((x & 0xF0F0F0F0) >> 4)
    x = ((x & 0x00FF00FF) << 8) | ((x & 0xFF00FF00) >> 8)
    x = ((x & 0x0000FFFF) << 16) | ((x & 0xFFFF0000) >> 16)
    return x

test_pulse_on = True
#####################
# START HERE
#####################
SendCommand_CTRL("global_reset")
sleep(0.1)

#####################
# tlu enable: 0,1
# tlu mode: 0 - no handshake, 1 - simple handshake, 2 - data hanshake
#####################
tlu_enable = 0
tlu_handshake_mode = 2
fc7.write("cnfg_tlu_handshake_mode", tlu_handshake_mode)
fc7.write("cnfg_tlu_enabled", tlu_enable)

################
## dio5 config #
################
# enabled outputs
out_en = [1,0,1,0,0]
# 50ohm termination
term_en = [0, 1, 0, 1, 1]
# thresholds
thresholds = [0,50,0,50,50]
################
#InitFMCPower("fmc_l8")
#Configure_DIO5(out_en, term_en, thresholds)

################
## fast config #
################
# trigger_source: 1 - L1, 2 - Stubs Coincidence, 3 - User Frequency, 4 - TLU, 5 - External from DIO5, 6 - triggers from test pulse sending machine
trigger_source = 3
if test_pulse_on == True:
	trigger_source = 6
# triggers_to_accept: 0 - continious triggering, otherwise sends neeeded amount and turns off
triggers_to_accept = 100
# trigger_user_frequency: in kHz 1 to 1000.
trigger_user_frequency = 1
# trigger_stubs_mask: can set a stubs coincidence, 5 means that stubs from hybrids id=2 and id=0 are required: 1b'101
trigger_stubs_mask = 5
# stub trigger latency
trigger_stub_delay = 195
#so called stub latency
trigger_stub_latency = 40

################
##CHIP config###
################
chip_type = "CBC_real"
header_size = 6
n_chips = 2
payload_size = 0 # this depends on the chip type and is calculated from the IPBus package you send.

if(chip_type == "SSA_emulator"):
	#SendCommand_I2C(command type, hybrid_id, chip_id, page, read, register_address, data, ReadBack)
	# command type: 0 - write to certain chip, hybrid; 1 - write to all chips on hybrid; 2 - write to all chips/hybrids (same for READ)
	#write to register 1 0xff as data --> this will put a 1 on the last 8 channels which go out
	SendCommand_I2C(            2,         0,       0,    0,     0,               1,    0xff,       0)
	#write to register 16 0xaa as data --> this will put a 0xaa on the last 8 bits of the HIP data which goes out
	SendCommand_I2C(            2,         0,       0,    0,     0,               16,    0xaa,       0)
	#write to register 33 1 as data --> this will put different data on the SSA centroid lines
	SendCommand_I2C(            2,         0,       0,    0,     0,               33,    1,       0)
	sleep(0.5)
	payload_size = 7
elif(chip_type == "CBC_emulator"):
	print "Doing nothing to configure the CBC emulator"	
	payload_size = 11
elif(chip_type == "CBC_real"):
	CBC_ConfigTXT('FE0CBC0.txt',0)#'Cbc2_default_hole.txt' or 2CBC2_hybrid_calibrated_CBC0.txt 
	CBC_ConfigTXT('FE0CBC1.txt',1)#'Cbc2_default_hole.txt' or 2CBC2_hybrid_calibrated_CBC1.txt
	payload_size = 11

################
#configure non default settings#####
################
SendCommand_I2C(            2      ,   0,       0,    0,     0,               2,    0xE7,        0) #register2: hitdetectSLVS, A7 (single); E7(variable)

SendCommand_I2C(            2      ,   0,       0,    0,     0,               15,   0xA0,        0) #register 15: MiscTestPulseCtrl&AnalogMux, set everything as below except for enbabling the TP (enable positive going test pulse)	
SendCommand_I2C(            2      ,   0,       0,    0,     0,               1,     199,        0) #register 1: TriggerLatency	
SendCommand_I2C(            2      ,   0,       0,    0,     0,               13,    0x40,        0) #register 13: test pulse potentiometer
if test_pulse_on == True:
	TestPulseDelay = int(sys.argv[1])
	print "int TestPulseDelay: ", TestPulseDelay
	TestPulseDelayRevBit = reverse_mask(TestPulseDelay) >> 27
	TestPulseDelayAndChGroup = TestPulseDelayRevBit << 3 
	print "TestPulseDelay ", bin(TestPulseDelay).zfill(8)
	print "TestPulseDelayRevBit ", bin(TestPulseDelayRevBit).zfill(8)
	print "TestPulseDelayAndChGroup ", bin(TestPulseDelayAndChGroup).zfill(8)
	SendCommand_I2C(            2      ,   0,       0,    0,     0,               15,    0xE0,        0) #register 15: MiscTestPulseCtrl&AnalogMux, 0x70 (enable positive going test pulse)	
	SendCommand_I2C(            2      ,   0,       0,    0,     0,               14,    TestPulseDelayAndChGroup,        0) 	
	SendCommand_I2C(            2      ,   0,       0,    0,     0,               13,    0x40,        0) #register 13: test pulse potentiometer
	
	fc7.write("cnfg_fast_delay_after_fast_reset" , 50)
	fc7.write("cnfg_fast_delay_after_test_pulse" , 200)
   	fc7.write("cnfg_fast_delay_before_next_pulse", 400)
	sleep(1)




n_words = header_size+n_chips*payload_size
################
##config fast###
################

fc7.write("cnfg_readout_common_stubdata_delay", trigger_stub_delay)
sleep(0.001)

Configure_Fast(triggers_to_accept, trigger_user_frequency, trigger_source, trigger_stubs_mask, trigger_stub_latency)
ReadStatus()


output_file  = open("Variable_no_TP.csv","w") 
for VCTH in range (100,254,1):
	print "--------------------------------------------------------------------------------------------"
	print "setting VCTH to : ", VCTH 
 	SendCommand_I2C(            2      ,   0,       0,    0,     0,               12,    VCTH,        0)	
	sleep(0.1)
	SendCommand_CTRL("start_trigger") 

	################
	##taking data###
	################

#	print "Waiting for triggers:"
	i = 0
	total_hits = 0
	while((triggers_to_accept>0 and i<triggers_to_accept) or (triggers_to_accept==0)):
		if (fc7.read("words_cnt") >= n_words):
			REC_DATA = fc7.fifoRead("ctrl_readout_run_fifo",n_words)
	#		print uInt32HexListStr(REC_DATA)
			#print REC_DATA
			hit = bin(((REC_DATA[24]) & 0x00000001)>>0) 
	#		print "channel 0 data: ", hit
			total_hits = total_hits + int(hit,2)
			#print_data(chip_type,n_words, n_chips, 1, REC_DATA)
			i=i+1
	occupancy = float(total_hits)/float(triggers_to_accept)
#	print "Number of words left: ", fc7.read("words_cnt")
	output_string = str(str(VCTH)+ ", "+ str(occupancy)+"\n")
	print output_string
	output_file.write(output_string)

output_file.close()



