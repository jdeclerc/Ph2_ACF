#include "TStyle.h"
#include "TH1.h"
#include "TGaxis.h"

#include <TTree.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1I.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <THStack.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TString.h>
#include <TLegend.h>
#include "TLegendEntry.h"
#include "TH1.h"
#include "TList.h"
#include "Math/WrappedMultiTF1.h"
#include <TObjArray.h>
#include <TString.h>
#include <vector>
#include "TF2.h"
#include "TH2.h"
#include <fstream>
#include <TGraph.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

template <typename T>

  std::string NumberToString ( T Number )
  {
     std::ostringstream ss;
     ss << Number;
     return ss.str();
  }


//void create2DPulseShape(const char * hName, bool normalize, string  partition)
void create2DPulseShape()
{
   //example of macro illustrating how to superimpose two histograms
   //with different scales in the "same" pad.
   // To see the output of this macro, click begin_html <a href="gif/twoscales.gif" >here</a> end_html
   //Author: Rene Brun

   TFile *OutputFile = new TFile("2D_pulseshape.root","RECREATE");
   bool fitted = false; //false means not fitted so pedestal is from the derivative 
   TCanvas *c1 = new TCanvas("c1","c1",600,400);
   gStyle->SetOptStat(kFALSE);


   //TFile* f1 = new TFile("../Results/PulseShape_Hole_06-06-18_10:16/PulseShape.root");
   TFile* f1 = new TFile("../Results/PulseShape_Hole_15-06-18_19:49/PulseShape.root");

   int min_channels = 1;
   int max_channels = 254;
   int min_time = 5000;
   int max_time = 5324;
   int min_VCTH = 0;
   int max_VCTH = 256;
   int time_step = 1;
   int testgroup = 1;
   int max_delay_bin = (max_time-min_time)/time_step;
   TH2D* h_2DPulseShape = new TH2D("h_2DPulseShape","h_2DPulseShape;channel;time; pedestal (VCTH)", max_channels-min_channels, min_channels, max_channels, max_time - min_time -1, min_time, max_time);
   TH2D* h_2DPulseShape_baseline_subtr = new TH2D("h_2DPulseShape_baseline_subtr","h_2DPulseShape_baseline_subtr;channel;time;pedestal (VCTH)", max_channels-min_channels, min_channels, max_channels, max_time - min_time-1, min_time, max_time -1);
   TH2D* h_2DPulseShape_baseline_subtr_no_TP_channels = new TH2D("h_2DPulseShape_baseline_subtr_no_TP_channels","h_2DPulseShape_baseline_subtr_no_TP;channel;time;pedestal (VCTH)", max_channels-min_channels, min_channels, max_channels, max_time - min_time -1, min_time, max_time-1);
   TH2D* h_2DPulseShape_baseline_subtr_no_TP_channels_odd = new TH2D("h_2DPulseShape_baseline_subtr_no_TP_channels_odd","h_2DPulseShape_baseline_subtr_no_TP_odd;channel;time;pedestal (VCTH)", max_channels-min_channels, min_channels, max_channels, max_time - min_time -1, min_time, max_time-1);
   TH2D* h_2DPulseShape_baseline_subtr_no_TP_channels_even = new TH2D("h_2DPulseShape_baseline_subtr_no_TP_channels_even","h_2DPulseShape_baseline_subtr_no_TP_even;channel;time;pedestal (VCTH)", max_channels-min_channels, min_channels, max_channels, max_time - min_time -1, min_time, max_time-1);
   
   TH2D* h_2DPulseShape_even = new TH2D("h_2DPulseShape_even","h_2DPulseShape_even;channel;time;pedestal (VCTH)", max_channels-min_channels, min_channels, max_channels, max_time - min_time -1, min_time, max_time-1);
   TH2D* h_2DPulseShape_odd = new TH2D("h_2DPulseShape_odd","h_2DPulseShape_odd;channel;time;pedestal (VCTH)", max_channels-min_channels, min_channels, max_channels, max_time - min_time -1, min_time, max_time-1);
   
   vector<double> v_maximum_pedestals;
   vector<double> v_minimum_pedestals;
   vector<double> v_channels;
   vector<double> v_number_below_start_pedestal;
   vector<double> v_number_above_start_pedestal;

   for(int i_delay = min_time; i_delay<max_time;i_delay++){	
	   string rootFile = "Delay"+NumberToString(i_delay);
           cout << "rootFile" << rootFile << endl;
     	   f1->cd(rootFile.c_str());
	 
	   for(int i_channel = min_channels; i_channel< max_channels; i_channel++){
		   float pedestal = 0.;
		   string h_name;
		   
		   TH1F *s_curve_derivative;
		   TF1 *s_curve;
		   if(!fitted){
			   h_name = "fDerivative_Be0_Fe0_Cbc0_Channel"+NumberToString(i_channel)+"Delay"+NumberToString(i_delay);
			   s_curve_derivative = (TH1F*)gDirectory->Get(h_name.c_str());
			   pedestal = s_curve_derivative->GetMean();
		   }
		   else{
			h_name = "Fit_Be0_Fe0_Cbc0_Channel"+NumberToString(i_channel)+"Delay"+NumberToString(i_delay);
			s_curve = (TF1*)gDirectory->Get(h_name.c_str());
			pedestal = s_curve->GetX(0.5,min_VCTH,max_VCTH);	
		   }


		   h_2DPulseShape->SetBinContent(i_channel, i_delay-min_time, pedestal);
		   if(i_channel%2==0)h_2DPulseShape_even->SetBinContent(i_channel, i_delay-min_time, pedestal);
	  	   else h_2DPulseShape_odd->SetBinContent(i_channel, i_delay-min_time, pedestal);	

	   }		   
   }

   //getting the projection along the channel axis for time 0   
   TH1D *h_proj_X_2DPulseShape = new TH1D("h_proj_X_2DPulseShape","h_proj_X_2DPulseShape;channel",max_channels-min_channels,min_channels,max_channels);
   for(int i_channel = min_channels; i_channel< max_channels; i_channel++){
	h_proj_X_2DPulseShape->SetBinContent(i_channel,h_2DPulseShape->GetBinContent(i_channel,0));
   }
   
   
   //normalize the h_2DPulseShape plot by subtracting the minimal value in each column, so for each row
   for(int i_channel = min_channels; i_channel< max_channels; i_channel++){

      TH1D *h_proj_Y_2DPulseShape = h_2DPulseShape->ProjectionY("a",i_channel,i_channel);
      double min_pedestal = h_proj_Y_2DPulseShape->GetMinimum();
      double max_pedestal = h_proj_Y_2DPulseShape->GetMaximum();
      v_minimum_pedestals.push_back(min_pedestal);
      v_maximum_pedestals.push_back(max_pedestal);
      double start_pedestal = h_proj_X_2DPulseShape->GetBinContent(i_channel);
      v_channels.push_back(i_channel);
      int number_below_start_pedestal = 0;
      int number_above_start_pedestal = 0;
       	for(int i_delay = 0; i_delay<max_delay_bin;i_delay++){
		double pedestal = h_proj_Y_2DPulseShape->GetBinContent(i_delay);
		h_2DPulseShape_baseline_subtr->SetBinContent(i_channel, i_delay,pedestal-start_pedestal);
		int idx = i_channel/16;
		int den1 = testgroup*2 + 1 + idx*16;
		int den2 = testgroup*2 + 1 + idx*16 +1;
		if( i_channel != (den1) && i_channel != (den2 )  )h_2DPulseShape_baseline_subtr_no_TP_channels->SetBinContent(i_channel, i_delay,pedestal-start_pedestal);
		if( i_channel != (den1) && i_channel != (den2 )  && i_channel % 2 == 0 )h_2DPulseShape_baseline_subtr_no_TP_channels_even->SetBinContent(i_channel, i_delay,pedestal-start_pedestal);
		if( i_channel != (den1) && i_channel != (den2 )  && i_channel % 2 == 1 )h_2DPulseShape_baseline_subtr_no_TP_channels_odd->SetBinContent(i_channel, i_delay,pedestal-start_pedestal);
		if(pedestal<start_pedestal){number_below_start_pedestal++ ; cout << "below start pedestal" << endl;}
		if(pedestal>start_pedestal)number_above_start_pedestal++;

      }
      v_number_below_start_pedestal.push_back(number_below_start_pedestal);
      v_number_above_start_pedestal.push_back(number_above_start_pedestal);

   }

   cout<<v_number_below_start_pedestal[5] << endl;
cout<<v_number_below_start_pedestal[55] << endl;
cout<<v_number_below_start_pedestal[10] << endl;
cout<<v_number_below_start_pedestal[15] << endl;
cout<<v_number_below_start_pedestal[20] << endl;

   TGraphErrors* g_channel_max_amplitude = new TGraphErrors(v_channels.size(),&v_channels[0],&v_maximum_pedestals[0],0,0);
   g_channel_max_amplitude->SetTitle("pulse shape maxima per channel");
   g_channel_max_amplitude->GetXaxis()->SetTitle("channel");
   g_channel_max_amplitude->GetYaxis()->SetTitle("pulse shape max");
   TGraphErrors* g_channel_min_amplitude = new TGraphErrors(v_channels.size(),&v_channels[0],&v_minimum_pedestals[0],0,0);
   g_channel_min_amplitude->SetTitle("pulse shape minima per channel");
   g_channel_min_amplitude->GetXaxis()->SetTitle("channel");
   g_channel_min_amplitude->GetYaxis()->SetTitle("pulse shape min");
   TGraphErrors* g_channel_number_below_start_pedestal = new TGraphErrors(v_channels.size(),&v_channels[0],&v_number_below_start_pedestal[0],0,0);
   g_channel_number_below_start_pedestal->SetTitle("number below start pedestal per channel");
   g_channel_number_below_start_pedestal->GetXaxis()->SetTitle("channel");
   g_channel_number_below_start_pedestal->GetYaxis()->SetTitle("number of times below start pedestal");
   TGraphErrors* g_channel_number_above_start_pedestal = new TGraphErrors(v_channels.size(),&v_channels[0],&v_number_above_start_pedestal[0],0,0);
   g_channel_number_above_start_pedestal->SetTitle("number above start pedestal per channel");
   g_channel_number_above_start_pedestal->GetXaxis()->SetTitle("channel");
   g_channel_number_above_start_pedestal->GetYaxis()->SetTitle("number of times above start pedestal");

   //h_2DPulseShape->Draw("colz");
   h_2DPulseShape_baseline_subtr_no_TP_channels->Draw("colz");
   OutputFile->cd();
   h_2DPulseShape->Write();
   h_2DPulseShape_baseline_subtr->Write();
   h_2DPulseShape_even->Write();
   h_2DPulseShape_odd->Write();
   h_2DPulseShape_baseline_subtr_no_TP_channels->Write();
   h_2DPulseShape_baseline_subtr_no_TP_channels_odd->Write();
   h_2DPulseShape_baseline_subtr_no_TP_channels_even->Write();

   g_channel_max_amplitude->Write();
   g_channel_min_amplitude->Write();
   g_channel_number_below_start_pedestal->Write();
   g_channel_number_above_start_pedestal->Write();
}

