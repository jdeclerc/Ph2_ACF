#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import numpy as np
import serial

import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import time

#f=open("uncal_map.txt")
b=int(115200*1.0)
print (b)
uart=serial.Serial("/dev/ttyACM1", b, )

#uart.read(65536)
#vmin=Vmin, vmax=Vmin+Vrange
Vmin=20
Vrange=6

#for line in 
if 0: 
    while(1):
        val=[]
    while(1):
        line=uart.readline()
        #print line
        if "new" in line: break
        try:
            n=int(line,10) #remove trailing \n
        except ValueError:
            continue
        #print "i",n
        if n>32768: n-=65536
        #print "o",n
        val.append(n)
        
        
    
    if len(val)!=64:continue
    print (val)
    
    a=np.reshape(val,(16,4))
    plt.imshow(a, cmap='hot', interpolation='nearest')
    plt.pause(0.05)
    animation.save("thermal_map.mp4",write=writer,fps=2)
    
    #plt.show()
    
fig = plt.figure()

#fig.set_size_inches(8,6,True)
frames=10
start=time.time()
#writer.setup(fig,"thermal_map.mp4",dpi=100)
#writer.set
maps=[]
begin_time=time.time()
for i in range(frames):
    val=[]        
    while(1):
        line=uart.readline()
        print( line)
        if bytes("Ta", 'ascii') in line:
            Tsplit=line[:-4].split(b'=')[1]
            print(Tsplit)
            #print(str(Tsplit).split(u'=') )
            #print(Tsplit.replace(b'=',b'')) 
            #Temp=float(line[:-4].split()[1][1:])
            try: 
                Temp=float(Tsplit.replace(b'=',b'') )
            except ValueError:
                Temp=float(Tsplit.split(b'C')[0][:-2])
                val.append(float(Tsplit.split(b'C')[1]))

                pass
        if bytes("new", 'ascii') in line:
            stop=time.time()
            print (stop-start, 1/(stop-start))
            start=time.time()
            break
        try:
            n=float(line) 
        except ValueError:
            continue
        #print "i",n
        #if n>32768: n-=65536
        #print "o",n
        val.append(n)
        
        #
    if len(val)!=64:continue
    print (sum(val)/len(val))
    maps.append((val,Temp))
    a=np.reshape(val,(16,4))
    a=a.transpose()
    if i%2==1:
        plt.clf()
        plt.imshow(a, cmap='hot', interpolation='nearest', vmin=Vmin, vmax=Vmin+Vrange)
        plt.annotate(u"Tsensor={5: 4.1f}°C\nmin={0: 4.1f}°C\nmax={1: 4.1f}°C, Δ={4: 4.1f}°C\navg={2: 4.1f}°C\nstddev={3: 4.1f}°C\n".format(np.min(a),np.max(a),np.mean(a),np.std(a),np.max(a)-np.min(a),Temp),xy=(0,0))
        plt.colorbar()
        plt.pause(0.05)
    
fps=frames/(time.time()-begin_time)
print ("fps", fps)
#exit(0)
j=0
plt.clf()
plt.rcParams['animation.ffmpeg_path'] = '/usr/local/bin/ffmpeg'
# Set up formatting for the movie files
Writer = animation.writers['ffmpeg']
writer = Writer(fps=fps,metadata=dict(artist='YGA'), bitrate=1800, codec="mpeg4")
#writer.setup(dpi=100)

with writer.saving(fig, "thermal_map.mp4", frames):
    for i,Temp in maps: #range(frames):
        j+=1
        print (j)
        print (i)

            
        
        #if len(val)==64:
        try:
            a=np.reshape(i,(16,4))
            a=a.transpose()
            
            #plt.imshow(a, cmap='hot', interpolation='bessel', vmin=14, vmax=24)
            plt.imshow(a, cmap='hot',interpolation='nearest', vmin=Vmin, vmax=Vmin+Vrange)
            plt.annotate(u"Tsensor={5: 4.1f}°C\nmin={0: 4.1f}°C\nmax={1: 4.1f}°C, Δ={4: 4.1f}°C\navg={2: 4.1f}°C\nstddev={3: 4.1f}°C\n".format(np.min(a),np.max(a),np.mean(a),np.std(a),np.max(a)-np.min(a),Temp),xy=(0,0))
            plt.colorbar()
            #fig.set_dpi(72)
            writer.grab_frame()
            plt.pause(0.05)
            plt.clf()
        except ValueError:
            pass
    
    
    
