#include "HybridTester.h"
#include <ctime>

// fill the Histograms, count the hits and increment Vcth
struct HistogramFiller  : public HwDescriptionVisitor
{
    TH1F* fBotHist;
    TH1F* fTopHist;
    const Event* fEvent;

    HistogramFiller ( TH1F* pBotHist, TH1F* pTopHist, const Event* pEvent ) : fBotHist ( pBotHist ), fTopHist ( pTopHist ), fEvent ( pEvent ) {}

    void visit ( Cbc& pCbc )
    {
        std::vector<bool> cDataBitVector = fEvent->DataBitVector ( pCbc.getFeId(), pCbc.getCbcId() );

        for ( uint32_t cId = 0; cId < NCHANNELS; cId++ )
        {
            if ( cDataBitVector.at ( cId ) )
            {
                uint32_t globalChannel = ( pCbc.getCbcId() * 254 ) + cId;

                //              LOG(INFO) << "Channel " << globalChannel << " VCth " << int(pCbc.getReg( "VCth" )) ;
                // find out why histograms are not filling!
                if ( globalChannel % 2 == 0 )
                    fBotHist->Fill ( globalChannel / 2 );
                else
                    fTopHist->Fill ( ( globalChannel - 1 ) / 2 );

            }
        }
    }
};

void HybridTester::ReconfigureCBCRegisters(std::string pDirectoryName )
{
    bool cCheck;
    bool cHoleMode;
    auto cSetting = fSettingsMap.find ( "HoleMode" );

    if ( cSetting != std::end ( fSettingsMap ) )
    {
        cCheck = true;
        cHoleMode = ( cSetting->second == 1 ) ? true : false;
    }

    std::string cMode;

    if ( cCheck )
    {
        if ( cHoleMode ) cMode = "hole";
        else cMode = "electron";
    }

    

    for (auto& cBoard : fBoardVector)
    {
        fBeBoardInterface->CbcHardReset ( cBoard );
        for (auto& cFe : cBoard->fModuleVector)
        {
            for (auto& cCbc : cFe->fCbcVector)
            {
                std::string pRegFile ;
                char buffer[120];
                if( pDirectoryName.empty() )
                {
                    sprintf(buffer, "%s/FE%dCBC%d.txt" , fDirectoryName.c_str() , cCbc->getFeId(), cCbc->getCbcId() );
                }
                else
                {
                    sprintf(buffer, "%s/FE%dCBC%d.txt" , pDirectoryName.c_str() , cCbc->getFeId(), cCbc->getCbcId() );
                }
    			
    			pRegFile = buffer;
                cCbc->loadfRegMap(pRegFile);
                fCbcInterface->ConfigureCbc ( cCbc );
                LOG (INFO) << GREEN << "\t\t Successfully reconfigured CBC" << int ( cCbc->getCbcId() ) << "'s regsiters from " << pRegFile << " ." << RESET ;
            }
        }

        //CbcFastReset as per recommendation of Mark Raymond
        fBeBoardInterface->CbcFastReset ( cBoard );
    }
}

void HybridTester::InitializeHists()
{
	TString cFrontName( "fHistTop" );
	fHistTop = ( TH1F* )( gROOT->FindObject( cFrontName ) );
	if ( fHistTop ) delete fHistTop;

	fHistTop = new TH1F( cFrontName, "Front Pad Channels; Pad Number; Occupancy [%]", ( fNCbc / 2 * 254 ), -0.5, ( fNCbc / 2 * 254 ) - 0.5 );
	fHistTop->SetFillColor( 4 );
	fHistTop->SetFillStyle( 3001 );

	TString cBackName( "fHistBottom" );
	fHistBottom = ( TH1F* )( gROOT->FindObject( cBackName ) );
	if ( fHistBottom ) delete fHistBottom;

	fHistBottom = new TH1F( cBackName, "Back Pad Channels; Pad Number; Occupancy [%]", ( fNCbc / 2 * 254 ), -0.5, ( fNCbc / 2 * 254 ) - 0.5 );
	fHistBottom->SetFillColor( 4 );
	fHistBottom->SetFillStyle( 3001 );
	
	TString cFrontNameMerged( "fHistTopMerged" );
	fHistTopMerged = ( TH1F* )( gROOT->FindObject( cFrontNameMerged ) );
	if ( fHistTopMerged ) delete fHistTopMerged;

	fHistTopMerged = new TH1F( cFrontNameMerged, "Front Pad Channels; Pad Number; Occupancy [%]", ( fNCbc / 2 * 254 ) , -0.5, ( fNCbc / 2 * 254 ) -0.5 );
	fHistTopMerged->SetFillColor( 4 );
	fHistTopMerged->SetFillStyle( 3001 );

	TString cBackNameMerged( "fHistBottomMerged" );
	fHistBottomMerged = ( TH1F* )( gROOT->FindObject( cBackNameMerged ) );
	if ( fHistBottomMerged ) delete fHistBottomMerged;

	fHistBottomMerged = new TH1F( cBackNameMerged, "Back Pad Channels; Pad Number; Occupancy [%]", ( fNCbc / 2 * 254 ) , -0.5, ( fNCbc / 2 * 254 ) - 0.5 );
	fHistBottomMerged->SetFillColor( 4 );
	fHistBottomMerged->SetFillStyle( 3001 );

	TString cOccupancyBottom("fHistOccupancyBottom"); 
	fHistOccupancyBottom = ( TH1F* )( gROOT->FindObject( cOccupancyBottom ) );
	if ( fHistOccupancyBottom ) delete fHistOccupancyBottom;
	fHistOccupancyBottom = new TH1F( cOccupancyBottom, "Back Pad Channels.", (int)( 110/1.0 ) , -0.5 , 110.0 - 0.5 );
	fHistOccupancyBottom->SetStats(1);
	fHistOccupancyBottom->SetFillColor( 4 );
	fHistOccupancyBottom->SetLineColor( 4 );
	fHistOccupancyBottom->SetFillStyle( 3004 );
	fHistOccupancyBottom->GetYaxis()->SetTitle("Number of Strips");
	fHistOccupancyBottom->GetXaxis()->SetTitle("Occupancy (%)");
	fHistOccupancyBottom->GetXaxis()->SetRangeUser(0.0,101.0);

	TString cOccupancyTop("fHistOccupancyTop"); 
	fHistOccupancyTop = ( TH1F* )( gROOT->FindObject( cOccupancyTop ) );
	if ( fHistOccupancyTop ) delete fHistOccupancyTop;
	fHistOccupancyTop = new TH1F( cOccupancyTop, "Top Pad Channels.", (int)( 110/1.0 ) , -0.5 , 110.0 - 0.5 );
	fHistOccupancyTop->SetStats(1);
	fHistOccupancyTop->SetFillColor( 3 );
	fHistOccupancyTop->SetLineColor( 3 );
	fHistOccupancyTop->SetFillStyle( 3004 );
	fHistOccupancyTop->GetYaxis()->SetTitle("Number of Strips");
	fHistOccupancyTop->GetXaxis()->SetTitle("Occupancy (%)");
	fHistOccupancyTop->GetXaxis()->SetRangeUser(0.0,101.0);
	



	// Now the Histograms for SCurves
	for ( auto cBoard : fBoardVector )
	  {
	    uint32_t cBoardId = cBoard->getBeId();
	    
	    for ( auto cFe : cBoard->fModuleVector )
	      {
		uint32_t cFeId = cFe->getFeId();
		
		for ( auto cCbc : cFe->fCbcVector )
		  {
		    
		    uint32_t cCbcId = cCbc->getCbcId();
		    
		    TString cName = Form( "SCurve_Fe%d_Cbc%d", cFeId, cCbcId );
		    TObject* cObject = static_cast<TObject*>( gROOT->FindObject( cName ) );
		    if ( cObject ) delete cObject;
		    TH1F* cTmpScurve = new TH1F( cName, Form( "Noise Occupancy Cbc%d; VCth; Counts", cCbcId ), 255, 0, 255 );
		    cTmpScurve->SetMarkerStyle( 8 );
			bookHistogram( cCbc, "Scurve", cTmpScurve );
			fSCurveMap[cCbc] = cTmpScurve;
		    
		    cName = Form( "SCurveFit_Fe%d_Cbc%d", cFeId, cCbcId );
		    cObject = static_cast<TObject*>( gROOT->FindObject( cName ) );
		    if ( cObject ) delete cObject;
		    TF1* cTmpFit = new TF1( cName, MyErf, 0, 255, 2 );
			bookHistogram( cCbc, "ScurveFit", cTmpFit );

		    fFitMap[cCbc] = cTmpFit;
			}
	      }
	  }
}

void HybridTester::InitialiseSettings()
{
    auto cSetting = fSettingsMap.find ( "Threshold_NSigmas" );
    fSigmas = ( cSetting != std::end ( fSettingsMap ) ) ? cSetting->second : 4;
    cSetting = fSettingsMap.find ( "Nevents" );
    fTotalEvents = ( cSetting != std::end ( fSettingsMap ) ) ? cSetting->second : 999;
    cSetting = fSettingsMap.find ( "HoleMode" );
    fHoleMode = ( cSetting != std::end ( fSettingsMap ) ) ? cSetting->second : true;


    // LOG(INFO) << "Read the following Settings: " ;
    // LOG(INFO) << "Hole Mode: " << fHoleMode << std::endl << "NEvents: " << fTotalEvents << std::endl << "NSigmas: " << fSigmas ;
}

void HybridTester::Initialize ( bool pThresholdScan )
{
	fThresholdScan = pThresholdScan;
	gStyle->SetOptStat( 000000 );
	//gStyle->SetTitleOffset( 1.3, "Y" );
	//  special Visito class to count objects
	Counter cCbcCounter;
	accept( cCbcCounter );
	fNCbc = cCbcCounter.getNCbc();

	fDataCanvas = new TCanvas( "fDataCanvas", "SingleStripEfficiency", 10, 0, 500, 500 );
	fDataCanvas->Divide( 2 );

	fSummaryCanvas = new TCanvas( "fSummaryCanvas", "Summarizing Module Efficiency", 10, 0, 500, 500 );
	fSummaryCanvas->Divide( 2 );


	if ( fThresholdScan )
	{
		fSCurveCanvas = new TCanvas( "fSCurveCanvas", "Noise Occupancy as function of VCth" );
		fSCurveCanvas->Divide( fNCbc );

	}
	InitializeHists();
	InitialiseSettings();

	fNoisyChannelsTop.clear();
	fNoisyChannelsBottom.clear();
	fDeadChannelsTop.clear();
	fDeadChannelsBottom.clear();
}

uint32_t HybridTester::fillSCurves ( BeBoard* pBoard,  const Event* pEvent, uint8_t pValue )
{
    uint32_t cHitCounter = 0;

    for ( auto cFe : pBoard->fModuleVector )
    {
        for ( auto cCbc : cFe->fCbcVector )
        {
            // SS
            /*TH1F* sCurveHist = static_cast<TH1F*>( getHist( cCbc, "Scurve" ) );
            uint32_t cbcEventCounter = 0;
            for ( uint32_t cId = 0; cId < NCHANNELS; cId++ )
            {
                if ( pEvent->DataBit( cCbc->getFeId(), cCbc->getCbcId(), cId ) )
                {
                    sCurveHist->Fill( pValue );
                    cHitCounter++;
                    cbcEventCounter++;
                }
            }*/

            auto cScurve = fSCurveMap.find ( cCbc );

            if ( cScurve == fSCurveMap.end() ) LOG (INFO) << "Error: could not find an Scurve object for Cbc " << int ( cCbc->getCbcId() ) ;
            else
            {
                for ( uint32_t cId = 0; cId < NCHANNELS; cId++ )
                {
                    if ( pEvent->DataBit ( cCbc->getFeId(), cCbc->getCbcId(), cId ) )
                    {
                        cScurve->second->Fill ( pValue );
                        cHitCounter++;
                    }
                }
            }
        }
    }

    return cHitCounter;
}

void HybridTester::ScanThresholds()
{
    LOG (INFO) << "Mesuring Efficiency per Strip ... " ;
    LOG (INFO) << "Taking data with " << fTotalEvents << " Events!" ;

    int cVcthStep = 2;
    uint8_t cVcth = ( fHoleMode ) ?  0xFF :  0x00;
    int cStep = ( fHoleMode ) ? (-1 * cVcthStep) : cVcthStep;

    int iVcth =  + (cVcth);
    //LOG(INFO) << RED << "Vcth = " <<  iVcth << RESET ;

    //simple VCth loop
    while ( 0 <= iVcth && iVcth <= 255 )
    {

        CbcRegWriter cWriter ( fCbcInterface, "VCth", cVcth );
        accept ( cWriter );

        fHistTop->GetYaxis()->SetRangeUser ( 0, fTotalEvents );
        fHistBottom->GetYaxis()->SetRangeUser ( 0, fTotalEvents );

        for ( BeBoard* pBoard : fBoardVector )
        {
            uint32_t cN = 1;
            uint32_t cNthAcq = 0;

            fBeBoardInterface->Start ( pBoard );

            while ( cN <=  fTotalEvents )
            {
                // Run( pBoard, cNthAcq );
                ReadData ( pBoard, false );
                const std::vector<Event*>& events = GetEvents ( pBoard );

                // Loop over Events from this Acquisition
                for ( auto& cEvent : events )
                {
                    HistogramFiller cFiller ( fHistBottom, fHistTop, cEvent );
                    pBoard->accept ( cFiller );

                    fillSCurves ( pBoard,  cEvent , cVcth );

                    if ( cN % 100 == 0 )
                    {
                        updateSCurveCanvas ( pBoard );
                        UpdateHists();
                    }

                    cN++;
                }

                cNthAcq++;
            }

            fBeBoardInterface->Stop ( pBoard);
        }

        fHistTop->Scale ( 100 / double_t ( fTotalEvents ) );
        fHistTop->GetYaxis()->SetRangeUser ( 0, 100 );
        fHistBottom->Scale ( 100 / double_t ( fTotalEvents ) );
        fHistBottom->GetYaxis()->SetRangeUser ( 0, 100 );
        UpdateHists();



        //LOG(INFO) << RED << "Vcth = " <<  iVcth << RESET << " " ;
        //LOG(INFO) << GREEN << "Mean occupancy at the Top side: " << fHistTop->Integral()/(double)(fNCbc*127) << RESET << " ";
        //LOG(INFO) << BLUE << "Mean occupancy at the Bottom side: " << fHistBottom->Integral()/(double)(fNCbc*127) << RESET ;
        iVcth = + (cVcth + cStep);
        cVcth += cStep;


    }

    // Fit and save the SCurve & Fit - extract the right threshold
    // TODO
    processSCurves ( fTotalEvents );

    //normalize noise scan
    /*for ( BeBoard* pBoard : fBoardVector )
    {
        for ( auto cFe : pBoard->fModuleVector )
        {
            for ( auto cCbc : cFe->fCbcVector )
            {
                fSCurveCanvas->cd(cCbc->getCbcId()+1);
                TH1F* sCurveHist = static_cast<TH1F*>( getHist( cCbc, "Scurve" ) );
                sCurveHist->Scale(100./(NCHANNELS*fTotalEvents));
                sCurveHist->GetYaxis()->SetTitle("Occupancy (%)");
                sCurveHist->GetYaxis()->SetTitleOffset(1.2);
                sCurveHist->DrawCopy("P0");

                sCurveHist->Write( sCurveHist->GetName(), TObject::kOverwrite );
                fSCurveCanvas->cd(cCbc->getCbcId()+1)->Update();
            }
        }
    }*/
}

int HybridTester::ReadMultiADCValues(int AMUX_setting){
 //if the antenna is not installed it does not make sense to also set the AMUX

 #ifdef __ANTENNA__

   int n_bit_DAC = 0;

   //AMUX setting: compare to table 18 in the CBC3 user manual
   if(AMUX_setting == 5) {//VCTH
	n_bit_DAC = 10;
	LOG(INFO) << "setting the n_bit_DAC to 10";
   }

   else if(AMUX_setting == 0b10000 || AMUX_setting == 0b10001){//Vplus2	or Vplus1
	n_bit_DAC = 4;
   }


   ThresholdVisitor cVisitor (fCbcInterface, 0);
   Antenna cAntenna;
   cAntenna.initializeAntenna();
   cAntenna.ConfigureADC(4);
   cAntenna.ConfigureClockGenerator(3, 8);
   cAntenna.ConfigureDigitalPotentiometer(2, 125); 
   sleep(1);

   int cbcIterator = 0;

   for (auto cBoard : this->fBoardVector)
    {
        for (auto cFe : cBoard->fModuleVector)
        {
	    //setting all the chips amux to 0, this is leave it floating	
            for (auto cCbc : cFe->fCbcVector)
            {
   		if(cCbc->getChipType() != ChipType::CBC3){LOG(INFO) << "This has only been implemented for CBC3, would have to change the AMUX settings and such"; return 1;} 
    	        int origValue = this->fCbcInterface->ReadCbcReg ( cCbc, "MiscTestPulseCtrl&AnalogMux" );
		int writeValue = origValue & 0b11100000; //keep the first 3 bits as is and set the others to 0
    	        this->fCbcInterface->WriteCbcReg ( cCbc, "MiscTestPulseCtrl&AnalogMux", writeValue,  true );
	    }

	    //making sure all chip AMUXes are at 0, this is overkill, but better safe then sorry	
            for (auto cCbc : cFe->fCbcVector)
            {
    	        int regContent = this->fCbcInterface->ReadCbcReg ( cCbc, "MiscTestPulseCtrl&AnalogMux" );
		if(regContent & 0b00011111 != 0) return 1;
		else LOG(INFO) << "AMUX register settings back to 0";
	    }

	    for (auto cCbc : cFe->fCbcVector)
	    {

	   
    	        int origValue = this->fCbcInterface->ReadCbcReg ( cCbc, "MiscTestPulseCtrl&AnalogMux" );
		//now for one chip change the AMUX setting, so it outputs something for a given DAC, but leave the first 3 bits untouched
		this->fCbcInterface->WriteCbcReg ( cCbc, "MiscTestPulseCtrl&AnalogMux", (origValue & 0b11100000) || (AMUX_setting & 0b00011111),  true );
		sleep(0.1);
		//now start looping over the different settings for this DAC and each time read the corresponding ADC value
		std::vector<double> v_DAC;
		std::vector<double> v_hybrT;
		std::vector<double> v_hybrI;
		std::vector<double> v_AMUXV;
                LOG(INFO) << "n_bit_DAC to be used in the scan " << n_bit_DAC;
		for(int i_DAC = 0; i_DAC < pow(2,n_bit_DAC); i_DAC++){
			LOG(INFO) << "we are at DAC setting " << i_DAC << " out of " << n_bit_DAC;
			if(AMUX_setting == 0b00101) {//VCTH
				cVisitor.setThreshold (i_DAC);
				cFe->accept (cVisitor);
				LOG(INFO)  << "Slow control values for VCTH setting: " << i_DAC ;
			}

			sleep(0.4);
			/*else if(AMUX_setting == 0b10000){//Vplus2
			}
			
			else if(AMUX_setting == 0b10001){//Vplus1

			}
			*/
			double hybrT = cAntenna.GetHybridTemperature();
			double hybrI = cAntenna.GetHybridCurrent();
			double AMUXV = cAntenna.GetAmuxVoltage(); 
			printf ("Hybrid Temperature = %.2f deg C\n", hybrT );
			printf ("Hybrid current = %.1f mA\n", hybrI );
			printf ("Amux Voltage = %.1f mV\n", AMUXV );

			v_DAC.push_back(i_DAC);
			v_hybrT.push_back(hybrT);
			v_hybrI.push_back(hybrI);
			v_AMUXV.push_back(AMUXV);
		}

		    TGraphErrors *g_AMUXV = new TGraphErrors(v_DAC.size(), &v_DAC[0], &v_AMUXV[0], 0, 0);
		    g_AMUXV->SetMarkerStyle(21);
		    std::string title = ("Amux measurement CBC"+std::to_string(int(cCbc->getCbcId()))+";DAC setting (LSB);ADC reading (mV)").c_str();
		    g_AMUXV->SetTitle(title.c_str());
			
		    TGraphErrors *g_hybrI = new TGraphErrors(v_DAC.size(), &v_DAC[0], &v_hybrI[0], 0, 0);
		    g_hybrI->SetMarkerStyle(21);
		    title = ("I measurement during measurement on CBC"+std::to_string(int(cCbc->getCbcId()))+";DAC setting (LSB);I (mA)").c_str();
		    g_hybrI->SetTitle(title.c_str());
			
		    TGraphErrors *g_hybrT = new TGraphErrors(v_DAC.size(), &v_DAC[0], &v_hybrT[0], 0, 0);
		    g_hybrT->SetMarkerStyle(21);
		    title = ("T measurement during measurement on CBC"+std::to_string(int(cCbc->getCbcId()))+";DAC setting (LSB);T (*C)").c_str();
		    g_hybrT->SetTitle(title.c_str());
			
		    fResultFile->cd();
		    g_AMUXV->Write();		
		    g_hybrI->Write();		
		    g_hybrT->Write();		

		    //write back amux setting 0 to this chip
    	            int origValue2 = this->fCbcInterface->ReadCbcReg ( cCbc, "MiscTestPulseCtrl&AnalogMux" );
	 	    int writeValue2 = origValue2 & 0b11100000; //keep the first 3 bits as is and set the others to 0
		    this->fCbcInterface->WriteCbcReg ( cCbc, "MiscTestPulseCtrl&AnalogMux", writeValue2,  true );
		    sleep(0.1);
		    int regContent = this->fCbcInterface->ReadCbcReg ( cCbc, "MiscTestPulseCtrl&AnalogMux" );
		    if(regContent & 0b00011111  != 0) return 1;
		    else LOG(INFO) << "AMUX register settings back to 0";
	     }
	    
	   }


	
	}
 return 0;
 #endif
}

void HybridTester::ScanThreshold()
{
    LOG (INFO) << "Scanning noise Occupancy to find threshold for test with external source ... " ;

    // Necessary variables
    uint32_t cEventsperVcth = 10;
    bool cNonZero = false;
    bool cAllOne = false;
    bool cSlopeZero = false;
    uint32_t cAllOneCounter = 0;
    uint32_t cSlopeZeroCounter = 0;
    uint32_t cOldHitCounter = 0;
    uint8_t  cDoubleVcth;
    uint8_t cVcth = ( fHoleMode ) ?  0xFF :  0x00;
    int cStep = ( fHoleMode ) ? -10 : 10;

    // Adaptive VCth loop
    while ( 0x00 <= cVcth && cVcth <= 0xFF )
    {
        if ( cAllOne ) break;

        if ( cVcth == cDoubleVcth )
        {
            cVcth +=  cStep;
            continue;
        }

        // Set current Vcth value on all Cbc's
        //CbcRegWriter cWriter ( fCbcInterface, "VCth", cVcth );
        //accept ( cWriter );

	 CbcRegWriter cWriter ( fCbcInterface, "VCth", cVcth );
	 accept ( cWriter );


        uint32_t cN = 1;
        uint32_t cNthAcq = 0;
        uint32_t cHitCounter = 0;

        // maybe restrict to pBoard? instead of looping?
        for ( BeBoard* pBoard : fBoardVector )
        {
            fBeBoardInterface->Start ( pBoard );

            while ( cN <=  cEventsperVcth )
            {
                // Run( pBoard, cNthAcq );
                ReadData ( pBoard, false );
                const std::vector<Event*>& events = GetEvents ( pBoard );

                // Loop over Events from this Acquisition
                for ( auto& cEvent : events )
                {
                    // loop over Modules & Cbcs and count hits separately
                    cHitCounter += fillSCurves ( pBoard,  cEvent, cVcth );
                    cN++;
                }

                cNthAcq++;
            }

            fBeBoardInterface->Stop ( pBoard);
            // LOG(INFO) << +cVcth << " " << cHitCounter ;
            // Draw the thing after each point
            updateSCurveCanvas ( pBoard );

            // check if the hitcounter is all ones

            if ( cNonZero == false && cHitCounter != 0 )
            {
                cDoubleVcth = cVcth;
                cNonZero = true;
                cVcth -= 2 * cStep;
                cStep /= 10;
                continue;
            }

            if ( cNonZero && cHitCounter != 0 )
            {
                // check if all Cbcs have reached full occupancy
                if ( cHitCounter > 0.95 * cEventsperVcth * fNCbc * NCHANNELS ) cAllOneCounter++;

                // add a second check if the global SCurve slope is 0 for 10 consecutive Vcth values
                // if ( fabs( cHitCounter - cOldHitCounter ) < 10 && cHitCounter != 0 ) cSlopeZeroCounter++;
            }

            if ( cAllOneCounter >= 10 ) cAllOne = true;

            // if ( cSlopeZeroCounter >= 10 ) cSlopeZero = true;

            if ( cAllOne )
            {
                LOG (INFO) << "All strips firing -- ending the scan at VCth " << +cVcth ;
                break;
            }

            // else if ( cSlopeZero )
            // {
            //   LOG(INFO) << "Slope of SCurve 0 -- ending the scan at VCth " << +cVcth ;
            //  break;
            // }

            cOldHitCounter = cHitCounter;
            cVcth += cStep;
        }
    }

    // Fit and save the SCurve & Fit - extract the right threshold
    // TODO
    processSCurves ( cEventsperVcth );
}

void HybridTester::processSCurves ( uint32_t pEventsperVcth )
{
    for ( auto cScurve : fSCurveMap )
    {
        fSCurveCanvas->cd ( cScurve.first->getCbcId() + 1 );

        cScurve.second->Scale ( 1 / double_t ( pEventsperVcth * NCHANNELS ) );
        cScurve.second->Draw ( "P" );
        // Write to file
        cScurve.second->Write ( cScurve.second->GetName(), TObject::kOverwrite );

        // Estimate parameters for the Fit
        double cFirstNon0 ( 0 );
        double cFirst1 ( 0 );

        // Not Hole Mode
        if ( !fHoleMode )
        {
            for ( Int_t cBin = 1; cBin <= cScurve.second->GetNbinsX(); cBin++ )
            {
                double cContent = cScurve.second->GetBinContent ( cBin );

                if ( !cFirstNon0 )
                {
                    if ( cContent ) cFirstNon0 = cScurve.second->GetBinCenter ( cBin );
                }
                else if ( cContent == 1 )
                {
                    cFirst1 = cScurve.second->GetBinCenter ( cBin );
                    break;
                }
            }
        }
        // Hole mode
        else
        {
            for ( Int_t cBin = cScurve.second->GetNbinsX(); cBin >= 1; cBin-- )
            {
                double cContent = cScurve.second->GetBinContent ( cBin );

                if ( !cFirstNon0 )
                {
                    if ( cContent ) cFirstNon0 = cScurve.second->GetBinCenter ( cBin );
                }
                else if ( cContent == 1 )
                {
                    cFirst1 = cScurve.second->GetBinCenter ( cBin );
                    break;
                }
            }
        }

        // Get rough midpoint & width
        double cMid = ( cFirst1 + cFirstNon0 ) * 0.5;
        double cWidth = ( cFirst1 - cFirstNon0 ) * 0.5;

        // find the corresponding fit
        auto cFit = fFitMap.find ( cScurve.first );

        if ( cFit == std::end ( fFitMap ) ) LOG (INFO) << "Error: could not find Fit for Cbc " << int ( cScurve.first->getCbcId() ) ;
        else
        {
            // Fit
            cFit->second->SetParameter ( 0, cMid );
            cFit->second->SetParameter ( 1, cWidth );

            cScurve.second->Fit ( cFit->second, "RNQ+" );
            cScurve.second->SetLineStyle (2);
            cFit->second->Draw ( "same" );

            // Write to File
            cFit->second->Write ( cFit->second->GetName(), TObject::kOverwrite );

            // TODO
            // Set new VCth - for the moment each Cbc gets his own Vcth - I shold add a mechanism to take one that works for all!
            double_t pedestal = cFit->second->GetParameter ( 0 );
            double_t noise = cFit->second->GetParameter ( 1 );

            uint8_t cThreshold = ceil ( pedestal + fSigmas * fabs ( noise ) );

            LOG (INFO) << "Identified a noise Occupancy of 50% at VCth " << static_cast<int> ( pedestal ) << " -- increasing by " << fSigmas <<  " sigmas (=" << fabs ( noise ) << ") to " << +cThreshold << " for Cbc " << int ( cScurve.first->getCbcId() ) ;

            TLine* cLine = new TLine ( cThreshold, 0, cThreshold, 1 );
            cLine->SetLineWidth ( 3 );
            cLine->SetLineColor ( kCyan );
            cLine->Draw ( "same" );

            fCbcInterface->WriteCbcReg ( cScurve.first, "VCth", cThreshold );
        }

    }

    fSCurveCanvas->Update();

#ifdef __HTTP__
    fHttpServer->ProcessRequests();
#endif

    // Write and Save the Canvas as PDF
    fSCurveCanvas->Write ( fSCurveCanvas->GetName(), TObject::kOverwrite );
    std::string cPdfName = fDirectoryName + "/NoiseOccupancy.pdf";
    fSCurveCanvas->SaveAs ( cPdfName.c_str() );
}

void HybridTester::updateSCurveCanvas ( BeBoard* pBoard )
{

    /*for ( auto cFe : pBoard->fModuleVector )
    {
        for ( auto cCbc : cFe->fCbcVector )
        {
            fSCurveCanvas->cd(cCbc->getCbcId()+1);
            TH1F* sCurveHist = static_cast<TH1F*>( getHist( cCbc, "Scurve" ) );
            sCurveHist->DrawCopy("P0");
            fSCurveCanvas->cd(cCbc->getCbcId()+1)->Update();
        }
    }*/

    // Here iterate over the fScurveMap and update
    fSCurveCanvas->cd();

    for ( auto cFe : pBoard->fModuleVector )
    {
        for ( auto cCbc : cFe->fCbcVector )
        {
            uint32_t cCbcId = cCbc->getCbcId();
            auto cScurve = fSCurveMap.find ( cCbc );

            if ( cScurve == fSCurveMap.end() ) LOG (INFO) << "Error: could not find an Scurve object for Cbc " << int ( cCbc->getCbcId() ) ;
            else
            {
                fSCurveCanvas->cd ( cCbcId + 1 );
                cScurve->second->DrawCopy ( "P" );
            }
        }
    }

    fSCurveCanvas->Update();

#ifdef __HTTP__
    fHttpServer->ProcessRequests();
#endif
}



void HybridTester::TestRegisters()
{
	// This method has to be followed by a configure call, otherwise the CBCs will be in an undefined state
    struct RegTester : public HwDescriptionVisitor
    {
        CbcInterface* fInterface;
        std::map<uint32_t, std::set<std::string>> fBadRegisters;
        RegTester ( CbcInterface* pInterface, uint32_t pNCbc ) : fInterface ( pInterface )
        {
            std::set<std::string> tempset;
            uint32_t cCbcIterator = 0;

            for (cCbcIterator = 0; cCbcIterator < pNCbc; cCbcIterator++)
                fBadRegisters[cCbcIterator] = tempset;
        }

        void visit ( Cbc& pCbc )
        {
            uint8_t cFirstBitPattern = 0xAA;
            uint8_t cSecondBitPattern = 0x55;

            CbcRegMap cMap = pCbc.getRegMap();

            for ( const auto& cReg : cMap )
            {
                if ( !fInterface->WriteCbcReg ( &pCbc, cReg.first, cFirstBitPattern, true ) ) fBadRegisters[pCbc.getCbcId()] .insert ( cReg.first );

                if ( !fInterface->WriteCbcReg ( &pCbc, cReg.first, cSecondBitPattern, true ) ) fBadRegisters[pCbc.getCbcId()] .insert ( cReg.first );
            }
        }

        void dumpResult ( std::string fDirectoryName )
        {
            ofstream report ( fDirectoryName + "/registers_test.txt" ); // Creates a file in the current directory
            report << "Testing Cbc Registers one-by-one with complimentary bit-patterns (0xAA, 0x55)" << std::endl;

            for ( const auto& cCbc : fBadRegisters )
            {
                report << "Malfunctioning Registers on Cbc " << cCbc.first << " : " << std::endl;

                for ( const auto& cReg : cCbc.second ) report << cReg << std::endl;

            }

            report.close();
            LOG (INFO) << "Channels diagnosis report written to: " + fDirectoryName + "/registers_test.txt" ;
        }
    };

    // This should probably be done in the top level application but there I do not have access to the settings map
    time_t start_time = time (0);
    char* start = ctime (&start_time);
    LOG (INFO) << "start: " << start ;
    LOG (INFO) << std::endl << "Running registers testing tool ... " ;
    RegTester cRegTester ( fCbcInterface, fNCbc );
    accept ( cRegTester );
    cRegTester.dumpResult ( fDirectoryName );
    LOG (INFO) << "Done testing registers, re-configuring to calibrated state!" ;
    start_time = time (0);
    char* stop = ctime (&start_time);
    LOG (INFO) << "stop: " << stop ;
    std::stringstream ss;
    ConfigureHw (ss);
    LOG (INFO) << ss.str();
}

void HybridTester::DisplayGroupsContent (std::array<std::vector<std::array<int, 5>>, 8> pShortedGroupsArray)
{
    std::stringstream ss;

    for ( int i = 0; i < 8; i++)
    {
        ss << "TP group ID: " << i << std::endl;

        for (auto cShortsVector : pShortedGroupsArray[i])
        {
            for (auto i : cShortsVector) ss << i << ' ';

            ss << std::endl;
        }

        ss << std::endl;
    }

    LOG (INFO) << ss.str();
}

std::vector<std::array<int, 2>> HybridTester::MergeShorts (std::vector<std::array<int, 2>> pShortA, std::vector<std::array<int, 2>> pShortB)
{
    std::stringstream ss;

    for (auto cChannel : pShortA)
    {
        if (!CheckChannelInShortPresence (cChannel, pShortB) )
            pShortB.push_back (cChannel);
    }

    for (auto cMemberChannel : pShortB)
    {
        for (auto i : cMemberChannel) ss << i << ' ';
    }

    ss << std::endl;
    LOG (INFO) << ss.str();

    return pShortB;
}

bool HybridTester::CheckShortsConnection (std::vector<std::array<int, 2>> pShortA, std::vector<std::array<int, 2>> pShortB)
{
    for (auto cChannel : pShortA)
    {
        if (CheckChannelInShortPresence (cChannel, pShortB) ) return true;
    }

    return false;
}

bool HybridTester::CheckChannelInShortPresence ( std::array<int, 2> pShortedChannel, std::vector<std::array<int, 2>> pShort)
{
    for (auto cChannel : pShort)
    {
        if (cChannel[0] == pShortedChannel[0] && cChannel[1] == pShortedChannel[1]) return true;
    }

    return false;
}

void HybridTester::ReconstructShorts (std::array<std::vector<std::array<int, 5>>, 8> pShortedGroupsArray)
{
    std::stringstream ss;
    ss << std::endl << "---------Creating shorted pairs-----------------" << std::endl;
    std::vector<std::vector<std::array <int, 2>>> cShortsVector;
    std::vector<std::array<int, 2>> cShort;
    std::array<int, 2> temp_shorted_channel;

    std::array<int, 2> best_candidate;
    int best_candidate_index = 0;
    int index = 0;
    int sub_index = 0;
    int cross_side_punishment = 2;
    int smallest_distance = 10000;
    bool matching_channel_found;

    for ( auto cShortedChannelsGroup : pShortedGroupsArray)
    {
        for (auto cShortedChannelInfo : cShortedChannelsGroup)
        {
            temp_shorted_channel[0] = cShortedChannelInfo[0];
            temp_shorted_channel[1] = cShortedChannelInfo[1];

            cShort.push_back (temp_shorted_channel);
            cShort.push_back (temp_shorted_channel);

            index = 0;
            matching_channel_found = false;

            for ( auto cCandidate : pShortedGroupsArray[cShortedChannelInfo[2]] )
            {
                if (cShortedChannelInfo[3] == cCandidate[2])
                {
                    if ( smallest_distance > (cross_side_punishment * (cShort[0][0] - cCandidate[0]) * (cShort[0][0] - cCandidate[0]) + (cShort[0][1] - cCandidate[1]) *  (cShort[0][1] - cCandidate[1]) ) )
                    {
                        smallest_distance = (cross_side_punishment * (cShort[0][0] - cCandidate[0]) * (cShort[0][0] - cCandidate[0]) + (cShort[0][1] - cCandidate[1]) * (cShort[0][1] - cCandidate[1]) );
                        best_candidate[0] = cCandidate[0];
                        best_candidate[1] = cCandidate[1];
                        best_candidate_index = index;
                        matching_channel_found = true;
                    }
                }

                index++;
            }

            if (matching_channel_found)
            {
                if (best_candidate[0] <= cShort[0][0] && best_candidate[1] < cShort[0][1] )
                {
                    cShort[0][0] = best_candidate[0];
                    cShort[0][1] = best_candidate[1];
                }

                else
                {
                    cShort[1][0] = best_candidate[0];
                    cShort[1][1] = best_candidate[1];
                }

                pShortedGroupsArray[cShortedChannelInfo[2]].erase (pShortedGroupsArray[cShortedChannelInfo[2]].begin() + best_candidate_index);

                for (auto cMemberChannel : cShort)
                {
                    for (auto i : cMemberChannel) ss << i << ' ';
                }

                ss << "smallest distance: " << smallest_distance << std::endl;
                //DisplayGroupsContent(pShortedGroupsArray);

                cShortsVector.push_back (cShort);
            }
            else ss << "ERROR: No matching channel found for detected short (watch the level of noise)!" << std::endl;

            smallest_distance = 10000;
            cShort.clear();
        }
    }

    ss << "---------Merging shorts-------------------------" << std::endl;
    index = cShortsVector.size();
    ss << "Number of shorted connections found: " << index << std::endl;

    while ( index > 1 )
    {
        index--;
        sub_index = index;

        while (sub_index > 0)
        {
            sub_index--;

            if (CheckShortsConnection ( cShortsVector[index], cShortsVector[sub_index]) )
            {
                cShortsVector[sub_index] = MergeShorts ( cShortsVector[index], cShortsVector[sub_index]);
                cShortsVector.erase (cShortsVector.begin() + index);
                break;
            }
        }
    }

    ss << "---------Outcome--------------------------------" << std::endl;

    int merged_shorts_counter = 0;
    for (auto someShort : cShortsVector)
    {
        merged_shorts_counter++;
        ss <<" - "<< merged_shorts_counter << " - ";
        ss << std::endl;
        for (auto cMemberChannel : someShort)
        {
            for (auto i : cMemberChannel) ss << i << ' ';
        }

        ss << std::endl;
    }

    LOG (INFO) << ss.str();
}

void HybridTester::SetBeBoardForShortsFinding (BeBoard* pBoard)
{
//    fBeBoardInterface->WriteBoardReg (pBoard, "COMMISSIONNING_MODE_RQ", 1 );
//    fBeBoardInterface->WriteBoardReg (pBoard, "COMMISSIONNING_MODE_CBC_TEST_PULSE_VALID", 1 );
//    fBeBoardInterface->WriteBoardReg (pBoard, "COMMISSIONNING_MODE_DELAY_AFTER_TEST_PULSE", 2 );

//    LOG (INFO) << "COMMISSIONNING_MODE_RQ: " << fBeBoardInterface->ReadBoardReg ( pBoard, "COMMISSIONNING_MODE_RQ" ) ;
//    LOG (INFO) << "COMMISSIONNING_MODE_CBC_TEST_PULSE_VALID: " << fBeBoardInterface->ReadBoardReg ( pBoard, "COMMISSIONNING_MODE_CBC_TEST_PULSE_VALID" ) ;
//    LOG (INFO) << "COMMISSIONNING_MODE_DELAY_AFTER_TEST_PULSE: " << fBeBoardInterface->ReadBoardReg ( pBoard, "COMMISSIONNING_MODE_DELAY_AFTER_TEST_PULSE" ) ;

    std::vector<std::pair<std::string, uint8_t>> cRegVec;

    if ( fHoleMode )
        cRegVec.push_back ( std::make_pair ( "MiscTestPulseCtrl&AnalogMux", 0xE1 ) );
    else
        cRegVec.push_back ( std::make_pair ( "MiscTestPulseCtrl&AnalogMux", 0x61 ) );

    //cRegVec.push_back ( std::make_pair ( "TestPulsePot", 0x40 ) );

    //cRegVec.push_back ( std::make_pair ( "VCth", 0x90 ) );

    //cRegVec.push_back ( std::make_pair ( "TriggerLatency", 0x01 ) );

    CbcMultiRegWriter cMultiWriter ( fCbcInterface, cRegVec );
    this->accept ( cMultiWriter );
}

void HybridTester::FindShorts()
{
    std::stringstream ss;
    uint8_t cGroupAddress[8] = {0, 4, 2, 6, 1, 5, 3, 7};
    uint8_t cTestPulseGroupId = 0;
    int number_shorts_gnd = 0;
    std::array<int, 5> cShortedChannelInfo;
    std::array<std::vector<std::array<int, 5>>, 8> cShortedGroupsArray;

    std::array<int,2> cGroundedChannel;
    std::vector<std::array<int,2> > cGroundedChannelsList;

    CbcRegReader cReader ( fCbcInterface, "VCth" );
    accept ( cReader );
    fHistTop->GetYaxis()->SetRangeUser ( 0, fTotalEvents );
    fHistBottom->GetYaxis()->SetRangeUser ( 0, fTotalEvents );

    for ( auto  cBoard : fBoardVector )
    {
        uint32_t cN = 1;
        uint32_t cNthAcq = 0;

        SetBeBoardForShortsFinding (cBoard);

        CbcRegWriter cWriter ( fCbcInterface, "SelTestPulseDel&ChanGroup", cGroupAddress[cTestPulseGroupId]);
        //CbcRegReader cReader( fCbcInterface, "SelTestPulseDel&ChanGroup" );
        ss << "\nShorted channels searching procedure\nSides: Top - 0\tBottom - 1 (Channel numbering starts from 0)\n" << std::endl;
        ss << "      Side\t| Channel_ID\t| Group_ID\t| Shorted_With_Group_ID" << std::endl;

        for (cTestPulseGroupId = 0; cTestPulseGroupId < 8; cTestPulseGroupId++)
        {
            cN = 1;
            cNthAcq = 0;

            cWriter.setRegister ( "SelTestPulseDel&ChanGroup", cGroupAddress[cTestPulseGroupId]);
            accept ( cWriter );
            //accept( cReader );

            fBeBoardInterface->Start ( cBoard );

            while ( cN <=  fTotalEvents )
            {
                //Run( cBoard, cNthAcq );
                ReadData ( cBoard, false );
                //fBeBoardInterface->ReadNEvents ( cBoard, cNthAcq );
                const std::vector<Event*>& events = GetEvents ( cBoard );

                // Loop over Events from this Acquisition
                for ( auto cEvent : events )
                {
                    HistogramFiller cFiller ( fHistBottom, fHistTop, cEvent );
                    cBoard->accept ( cFiller );

                    if ( cN % 100 == 0 )
                        UpdateHists();

                    cN++;
                }

                cNthAcq++;
            }

            fBeBoardInterface->Stop ( cBoard);

            std::vector<std::array<int, 5>> cShortedChannelsGroup;

            for ( uint16_t cChannelId = 1; cChannelId < fNCbc * 127 + 1; cChannelId++ )
            {
                if ( fHistTop->GetBinContent ( cChannelId ) > 0.5 * fTotalEvents && ( (cChannelId - 1) % 127) % 8 != cTestPulseGroupId )
                {
                    cShortedChannelInfo = {0, (cChannelId - 1), ( (cChannelId - 1) % 127) % 8, cTestPulseGroupId, 0};
                    cShortedChannelsGroup.push_back (cShortedChannelInfo);
                    ss << "\t0\t|\t" << cShortedChannelInfo[1] << "\t|\t" << cShortedChannelInfo[2] << "\t|\t" << cShortedChannelInfo[3] << std::endl;
                    fHistTopMerged->SetBinContent ( cChannelId, fHistTop->GetBinContent ( cChannelId ) );
                }
                if (fHistTop->GetBinContent( cChannelId ) < 0.5*fTotalEvents && ((cChannelId-1)%127)%8 == cTestPulseGroupId)
                {
                    cGroundedChannel = {0, (cChannelId-1)};                    
                    cGroundedChannelsList.push_back(cGroundedChannel);
                    ss<<"\t0\t|\t"<<(cChannelId-1)<<"\t|\t"<<(cTestPulseGroupId + 0)<<"\t|\tGND"<<std::endl;
                    number_shorts_gnd ++;
                }

                if ( fHistBottom->GetBinContent ( cChannelId ) > 0.5 * fTotalEvents && ( (cChannelId - 1) % 127) % 8 != cTestPulseGroupId)
                {
                    cShortedChannelInfo = {1, (cChannelId - 1), ( (cChannelId - 1) % 127) % 8, cTestPulseGroupId, 0};
                    cShortedChannelsGroup.push_back (cShortedChannelInfo);
                    ss << "\t1\t|\t" << cShortedChannelInfo[1] << "\t|\t" << cShortedChannelInfo[2] << "\t|\t" << cShortedChannelInfo[3] << std::endl;
                    fHistBottomMerged->SetBinContent ( cChannelId, fHistBottom->GetBinContent ( cChannelId ) );
                }
                if (fHistBottom->GetBinContent( cChannelId ) < 0.5*fTotalEvents && ((cChannelId-1)%127)%8 == cTestPulseGroupId)
                {
                    cGroundedChannel = {1, (cChannelId-1)};                    
                    cGroundedChannelsList.push_back(cGroundedChannel);
                    ss<<"\t1\t|\t"<<(cChannelId-1)<<"\t|\t"<<(cTestPulseGroupId + 0)<<"\t|\tGND"<<std::endl;
                    number_shorts_gnd ++;
                }

            }
            
            cShortedGroupsArray[cTestPulseGroupId] = cShortedChannelsGroup;
            //if (cTestPulseGroupId == 2) return;
            fHistBottom->Reset();
            fHistTop->Reset();
            ss << "------------------------------------------------------------------------" << std::endl;
            

        }
    }
    LOG (INFO) << ss.str();
    fHistTopMerged->Scale ( 100 / double_t ( fTotalEvents ) );
    fHistTopMerged->GetYaxis()->SetRangeUser ( 0, 100 );
    fHistBottomMerged->Scale ( 100 / double_t ( fTotalEvents ) );
    fHistBottomMerged->GetYaxis()->SetRangeUser ( 0, 100 );
    ReconstructShorts (cShortedGroupsArray);
    std::cout << std::endl <<" Number of grounded inputs: " << number_shorts_gnd << std::endl;
    
    UpdateHistsMerged();

    //LOG (INFO) << ss.str();
}

void HybridTester::Measure()
{
    LOG (INFO) << "Mesuring Efficiency per Strip ... " ;
    LOG (INFO) << "Taking data with " << fTotalEvents << " Events!" ;

    CbcRegReader cReader ( fCbcInterface, "VCth" );
    accept ( cReader );
    fHistTop->GetYaxis()->SetRangeUser ( 0, fTotalEvents );
    fHistBottom->GetYaxis()->SetRangeUser ( 0, fTotalEvents );



    for ( BeBoard* pBoard : fBoardVector )
    {
        uint32_t cN = 1;
        uint32_t cNthAcq = 0;

        fBeBoardInterface->Start ( pBoard );

        while ( cN <=  fTotalEvents )
        {
            //Run( pBoard, cNthAcq );
            ReadData ( pBoard, false );
            //fBeBoardInterface->ReadNEvents ( pBoard, cNthAcq );
            const std::vector<Event*>& events = GetEvents ( pBoard );

            // Loop over Events from this Acquisition
            for ( auto& cEvent : events )
            {
                HistogramFiller cFiller ( fHistBottom, fHistTop, cEvent );
                pBoard->accept ( cFiller );

                if ( cN % 100 == 0 )
                    UpdateHists();

                cN++;
            }

            cNthAcq++;
        }

        fBeBoardInterface->Stop ( pBoard);
    }
	for( int i = 1 ; i < ( fNCbc/2 * 254 ) ; i++ )
	{
		double cOccupancyTop = 100*fHistTop->GetBinContent(i)/(double)(fTotalEvents); 
		double cOccupancyBottom = 100*fHistBottom->GetBinContent(i)/(double)(fTotalEvents);
		fHistOccupancyBottom->Fill( cOccupancyBottom );
		fHistOccupancyTop->Fill( cOccupancyTop );
	}

	fHistTop->Scale( 100 / double_t( fTotalEvents ) );
	fHistTop->GetYaxis()->SetRangeUser( 0, 100 );
	fHistBottom->Scale( 100 / double_t( fTotalEvents ) );
	fHistBottom->GetYaxis()->SetRangeUser( 0, 100 );
	UpdateHists();
	
	LOG (INFO) << "\t\tMean occupancy for the Top side: " << fHistOccupancyTop->GetMean() << " ± " << fHistOccupancyTop->GetRMS() << RESET  ; 
	LOG (INFO) << "\t\tMean occupancy for the Botton side: " << fHistOccupancyBottom->GetMean() << " ± " <<  fHistOccupancyBottom->GetRMS()  << RESET ; 
	ClassifyChannels();
}
void HybridTester::ClassifyChannels(double pNoiseLevel , double pDeadLevel ) 
{
	for( int i = 1 ; i < ( fNCbc/2 * 254 ) ; i++ )
	{
		if( fHistBottom->GetBinContent(i) >= pNoiseLevel ) fNoisyChannelsBottom.push_back(i) ; 
		if( fHistTop->GetBinContent(i) >= pNoiseLevel ) fNoisyChannelsTop.push_back(i); 


		if( fHistBottom->GetBinContent(i) <= pDeadLevel ) fDeadChannelsBottom.push_back(i) ; 
		if( fHistTop->GetBinContent(i) <= pDeadLevel ) fDeadChannelsTop.push_back(i); 
	}	
}
void HybridTester::DisplayNoisyChannels(std::ostream& os)
{
	std::string line;

	line = "# Noisy channels on Bottom Sensor : "; 
	for( int i = 0 ; i <  fNoisyChannelsBottom.size() ; i++ ) 
	{	
		line += std::to_string( fNoisyChannelsBottom[i] ) ;
		line +=  ( i < fNoisyChannelsBottom.size()-1 ) ? "," : "" ;
	}
	os << line << std::endl;

	line = "# Noisy channels on Top Sensor : "; 
	for( int i = 0 ; i <  fNoisyChannelsTop.size() ; i++ ) 
	{	
		line += std::to_string( fNoisyChannelsTop[i] ) ;
		line +=  ( i < fNoisyChannelsTop.size()-1 ) ? "," : "" ;
	}
	os << line << std::endl;
}
void HybridTester::DisplayDeadChannels(std::ostream& os)
{
	std::string line;

	line = "# Dead channels on Bottom Sensor : "; 
	for( int i = 0 ; i <  fDeadChannelsBottom.size() ; i++ ) 
	{	
		line += std::to_string( fDeadChannelsBottom[i] ) ;
		line +=  ( i < fDeadChannelsBottom.size()-1 ) ? "," : "" ;
	}
	os << line << std::endl;

	line = "# Dead channels on Top Sensor : "; 
	for( int i = 0 ; i <  fDeadChannelsTop.size() ; i++ ) 
	{	
		line += std::to_string( fDeadChannelsTop[i] ) ;
		line +=  ( i < fDeadChannelsTop.size()-1 ) ? "," : "" ;
	}
	os << line << std::endl;
}
void HybridTester::PrintHybridParameters()
{
    #ifdef __ANTENNA__
        Antenna cAntenna;
        cAntenna.initializeAntenna();
        cAntenna.ConfigureADC(4);
        
    #endif
}




void HybridTester::AntennaScan()
{
#ifdef __ANTENNA__
    LOG (INFO) << "Mesuring Efficiency per Strip ... " ;
    LOG (INFO) << "Taking data with " << fTotalEvents << " Events!" ;

    CbcRegReader cReader ( fCbcInterface, "VCth" );
    accept ( cReader );

    Antenna cAntenna;
    cAntenna.initializeAntenna();
    cAntenna.ConfigureClockGenerator(3, 8);
    cAntenna.ConfigureDigitalPotentiometer(2, 125);
    const int max_delay = 128;
    double trigger_latency[max_delay];
    double occupancy_top[max_delay];
    double occupancy_bottom[max_delay];

    //for (int delay = 0; delay < max_delay; delay++)
    //{
       // CbcRegWriter cWriter ( fCbcInterface, "TriggerLatency", delay);   
       // accept(cWriter);

        for (int channel = 0; channel < fNCbc; channel++) cAntenna.ConfigureAnalogueSwitch ( channel );

        fHistTop->GetYaxis()->SetRangeUser ( 0, fTotalEvents );
        fHistBottom->GetYaxis()->SetRangeUser ( 0, fTotalEvents );

        for ( uint16_t analog_switch_cs = 0; analog_switch_cs < fNCbc; analog_switch_cs++ )
        {
            //LOG (INFO) << "Chip Select ID " << +analog_switch_cs ;
            cAntenna.ConfigureAnalogueSwitch ( analog_switch_cs );
            cAntenna.TurnOnAnalogSwitchChannel ( 9 );

            for ( uint16_t channel_position = 1; channel_position < 10; channel_position++ )
            {

                cAntenna.TurnOnAnalogSwitchChannel ( channel_position );
                cAntenna.TurnOnAnalogSwitchChannel ( channel_position );

            //if(channel_position%2 == 0)
            //{

                if (channel_position == 9) break;
                std::cout<<"AMUX"<<analog_switch_cs<<" channel: "<<channel_position<<std::endl;
                for ( BeBoard* pBoard : fBoardVector)
                {
                        uint32_t cN = 1;
                        uint32_t cNthAcq = 0;

                        fBeBoardInterface->Start ( pBoard );

                        while ( cN <=  fTotalEvents )
                        {
                            // Run( pBoard, cNthAcq );
                            ReadData ( pBoard, false );
                            const std::vector<Event*>& events = GetEvents ( pBoard );

                            // Loop over Events from this Acquisition
                            for ( auto& cEvent : events )
                            {
                                HistogramFiller cFiller ( fHistBottom, fHistTop, cEvent );
                                pBoard->accept ( cFiller );

                                if ( cN % 100 == 0 ) UpdateHists();

                                cN++;
                            }

                            cNthAcq++;
                        }

                        fBeBoardInterface->Stop ( pBoard);
                        
                        /*Here the reconstruction of histograms happens*/
                        for ( uint16_t channel_id = 1; channel_id < fNCbc * 127 + 1; channel_id++ )
                        {
                            if ( fHistTopMerged->GetBinContent ( channel_id ) < fHistTop->GetBinContent ( channel_id ) ) fHistTopMerged->SetBinContent ( channel_id, fHistTop->GetBinContent ( channel_id ) );

                            if ( fHistBottomMerged->GetBinContent ( channel_id ) < fHistBottom->GetBinContent ( channel_id ) ) fHistBottomMerged->SetBinContent ( channel_id, fHistBottom->GetBinContent ( channel_id ) );
                        }
                        //std::cin.ignore();
                        /*Here clearing histograms after each event*/
                        fHistBottom->Reset();
                        fHistTop->Reset();
                }

                
            }
            //}
        }

        
        fHistTopMerged->Scale ( 100 / double_t ( fTotalEvents ) );
        fHistTopMerged->GetYaxis()->SetRangeUser ( 0, 100 );
        fHistBottomMerged->Scale ( 100 / double_t ( fTotalEvents ) );
        fHistBottomMerged->GetYaxis()->SetRangeUser ( 0, 100 );
        UpdateHistsMerged();

        double cOccupancyTop;
        double cOccupancyBottom;
        for( int i = 1 ; i < ( fNCbc * 127 +1 ) ; i++ )
        {
            cOccupancyTop = 100*fHistTopMerged->GetBinContent(i)/(double)(fTotalEvents); 
            cOccupancyBottom = 100*fHistBottomMerged->GetBinContent(i)/(double)(fTotalEvents);
            fHistOccupancyBottom->Fill( cOccupancyBottom );
            fHistOccupancyTop->Fill( cOccupancyTop );
        }

        //Open search procedure in PS MCK hybrids
        int opens_counter = 0;
        std::cout<<"Open channels:";
        for (int i = 127; i<254; i++)
        {
            if (fHistTopMerged->GetBinContent(i+1)/(double)(fTotalEvents) < 0.1)
            {
                opens_counter++;
                std::cout<<" "<<i<<";";
            }
        }
        std::cout<<std::endl<<"Total number of open channels found in the hybrid: "<<opens_counter<<std::endl;

       // trigger_latency[delay] = delay*0.025;
       // occupancy_top[delay] = fHistOccupancyTop->GetMean();
       // occupancy_bottom[delay] = fHistOccupancyBottom->GetMean();
      //  std::cout<<"trigger delay: "<<delay<< " top: "<<occupancy_top[delay]<< " bottom: "<<occupancy_bottom[delay]<<std::endl;
        //cAntenna.close();
        
        //std::cin.ignore();
        //TestChannels ( fDecisionThreshold );

        //fHistBottomMerged->Reset();
        //fHistTopMerged->Reset();
   // }
//TGraph *g = new TGraph(max_delay,trigger_latency,occupancy_top); //where x , y are 2 arrays of n points
//g->Draw("ap");
	cAntenna.TurnOnAnalogSwitchChannel ( 9 ); //trun off analogue switch after the test otherwise keep inducing signal...
	cAntenna.TurnOnAnalogSwitchChannel ( 9 ); //trun off analogue switch after the test otherwise keep inducing signal...
	cAntenna.TurnOnAnalogSwitchChannel ( 9 ); //trun off analogue switch after the test otherwise keep inducing signal...
#endif
}

void HybridTester::SaveTestingResults (std::string pHybridId)
{

    ifstream infile;
    std::string line_buffer;
    std::string content_buffer;
    std::string date_string = currentDateTime();
    std::string filename = "Results/HybridTestingDatabase/Hybrid_ID" + pHybridId + "_on" + date_string + ".txt";
    ofstream myfile;
    myfile.open ( filename.c_str() );
    myfile << "Hybrid ID: " << pHybridId << std::endl;
    myfile << "Created on: " << date_string << std::endl << std::endl;
    myfile << " Hybrid Testing Report" << std::endl;
    myfile << "-----------------------" << std::endl << std::endl;
    myfile << " Write/Read Registers Test" << std::endl;
    myfile << "---------------------------" << std::endl;

    infile.open ( fDirectoryName + "/registers_test.txt" );

    while ( getline ( infile, line_buffer ) ) content_buffer += line_buffer + "\r\n"; // To get all the lines.

    if ( content_buffer == "" ) myfile << "Test not performed!" << std::endl;

    infile.close();
    myfile << content_buffer << std::endl;
    content_buffer = "";
    myfile << " Channels Functioning Test" << std::endl;
    myfile << "---------------------------" << std::endl;
    infile.open ( fDirectoryName + "/channels_test2.txt" );

    while ( getline ( infile, line_buffer ) ) content_buffer += line_buffer + "\r\n"; // To get all the lines.

    if ( content_buffer == "" ) myfile << "Test not performed!" << std::endl;

    infile.close();
    myfile << content_buffer << std::endl;
    myfile.close();
    LOG (INFO) << std::endl << "Summary testing report written to: " << std::endl << filename ;
}
void HybridTester::writeObjects()
{
	fResultFile->cd();
	fHistTop->Write( fHistTop->GetName(), TObject::kOverwrite );
	fHistBottom->Write( fHistBottom->GetName(), TObject::kOverwrite );
	fDataCanvas->Write( fDataCanvas->GetName(), TObject::kOverwrite );

    fHistOccupancyTop->Write( fHistOccupancyTop->GetName(), TObject::kOverwrite );
	fHistOccupancyBottom->Write( fHistOccupancyBottom->GetName(), TObject::kOverwrite );
	fSummaryCanvas->Write( fSummaryCanvas->GetName(), TObject::kOverwrite );

	//fResultFile->Write();
    //fResultFile->Close();
    LOG (INFO) << BOLDBLUE << "Results of occupancy measured written to " << fDirectoryName + "/Summary.root" << RESET ;

	std::string cPdfName = fDirectoryName + "/HybridTestResults.pdf";
	fDataCanvas->SaveAs( cPdfName.c_str() );
	cPdfName = fDirectoryName + "/NoiseOccupancySummary.pdf";
	fSummaryCanvas->SaveAs( cPdfName.c_str() );
	

	if ( fThresholdScan )
	{
		cPdfName = fDirectoryName + "/ThresholdScanResults.pdf";
		fSCurveCanvas->SaveAs( cPdfName.c_str() );
	}
}
