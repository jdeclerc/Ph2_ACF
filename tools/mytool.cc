#include "mytool.h"

mytool::mytool (uint8_t pChannel, uint8_t pTestGroup, int CST, int TMIN, int TMAX) :
    fChan (pChannel),
    fTestGroup (pTestGroup),
    cst(CST),
    tmin(TMIN),
    tmax(TMAX)


{
}

void mytool::Initialize()
{
    //let's start by parsing the settings
    this->parseSettings();

    //we want to keep things simple, so lets get a pointer to a CBC and a pointer to a BeBoard
    //this will facilitate the code as we save a lot of looping
    fBoard = this->fBoardVector.at (0);
    fCbc = fBoard->fModuleVector.at (0)->fCbcVector.at (0);

    //we also need a TCanvas
    //std::string cDirectory = ( cmd.foundOption ( "output" ) ) ? cmd.optionValue ( "output" ) : "Results/pulseshape";
    //std::string cDirectory = "Results/pulseshape";
    //if ( !cNoise ) cDirectory += "Commissioning";
    //    //else if ( cNoise ) cDirectory += "NoiseScan";
    //
    //        if ( cNoise )          cDirectory += "NoiseScan";
    //            else if ( cSignalFit ) cDirectory += "SignalFit";
    //                else                   cDirectory += "Commissioning";
    //
    //Tool cTool;
    //cTool.CreateResultDirectory ( cDirectory ,false , true);
    //LOG (INFO) << " AAAAAAAAA file name: "<<fDirectoryName;

    //TFile* ff;
    //ff= TFile::Open ( "out_" + , "RECREATE" );

    fCanvas = new TCanvas ("mytool", "mytool");
    //fCanvas->Divide (2, 1);

    fCanvas2 = new TCanvas ("tpampvsped", "tpampvsped");
    ftpvsped = new TH1F ("htpampvsped", "htpampvsped",500,0,500);
    //now let's enable exactly 1 channel by setting the offset to 80 decimal in electron mode or 170 in hole mode
    //the channel registers in the CBC file start from 1, our channels start from 0
    //Jarne: potential problem?
    this->fCbcInterface->WriteCbcReg (fCbc, Form ("Channel%03d", fChan + 1), (fHoleMode) ? 0xaa : 0x50 );


    fChannel = new Channel (fBoard->getBeId(), fCbc->getFeId(), fCbc->getCbcId(), fChan );
    fChannel->initializeHist (0, "VCth");
}

void mytool::SweepTPDelay()
{
    LOG (INFO) << "Start sweeping delays";
    //first, set up the CbcTestPulse
    //now, lets figure out the time window we want to sweep
    //default timing is: trigger 200 clock cycles after test pulse
    //therefore we probably want to loop from 0 clock cycles relative to that to +8 clock cycles
    //this corresponds to a time window of 200ns in steps of 5ns makes 40 bins

    int cCoarseDefault = 196;
    int cLow = ( cCoarseDefault + tmin ) * 25;
    int cHigh = ( cCoarseDefault + tmax ) * 25;
    int cSteps = cst;


    std::vector<Double_t> peakingTimes;
    std::vector<Double_t> TPamplitudes;
    std::vector<Double_t> TPamplitudesfC;
    std::vector<Double_t> pedestals;


    int maxTestPulseAmplitude = 40;
    int minTestPulseAmplitude = 0;
    int stepTestPulseAmplitude = 80;
    bool pulseShapeForEveryTPAmpl = true; //if true will do a full pulse shape measurement for every TP amplitude and use the optimal timing for each one. If false will use the same time for all.
    bool skipFindDelayForEveryAmplitude = false; //leave to false
    double peakingTime = 0;

    for(int testPulseAmplitude = maxTestPulseAmplitude; testPulseAmplitude >= minTestPulseAmplitude; testPulseAmplitude = testPulseAmplitude-stepTestPulseAmplitude ){
	    LOG(INFO) << "Changing Testpulse amplitude to ", testPulseAmplitude;
    	    this->setSystemTestPulse (testPulseAmplitude, fTestGroup, true, fHoleMode );
	    std::string fPulseName = "pulseshape_"+std::to_string(testPulseAmplitude);
            fPulse = new TH1F (fPulseName.c_str(), fPulseName.c_str(), (cHigh-cLow)/cSteps, 0, cHigh-cLow);
	    int cBin = 1;
	    if(!skipFindDelayForEveryAmplitude){
		    //loop
		    for (uint32_t cDelayns = cLow; cDelayns < cHigh; cDelayns += cSteps)
		    {
			//set the delays
			setDelayAndTestGroup (cDelayns);
			LOG (INFO) << "Setting delay to " << cDelayns << " ns and filling bin " << cBin;
			//measure the pedestal
			double cPedestal = this->measureSCurve();
			//fill our pulse shape histogram
			fPulse->SetBinContent (cBin, cPedestal);
			//draw it
			fCanvas->cd ();
			fPulse->Draw();
			fCanvas->Update();
			cBin++;
		    }
		    fCanvas2->cd();
		    LOG (INFO) << "Finished sweeping delays for one test pulse setting";
		    fPulse->Write();

		    int binmax = fPulse->GetMaximumBin(); peakingTime = binmax*cSteps + cLow; double pedestal = fPulse->GetBinContent(binmax);
		    LOG (INFO) <<"Found max Pedestal value "<< pedestal << " at time "<< peakingTime; 
		    ftpvsped->SetBinContent(400, pedestal);
		    ftpvsped->Draw("e2");
		    fCanvas2->Update(); 
		    TPamplitudes.push_back(testPulseAmplitude);
		    pedestals.push_back(pedestal); 
		    peakingTimes.push_back(peakingTime);
	   }	
	   if(pulseShapeForEveryTPAmpl == false){
		    //now check for other test pulse amplitudes at the same time what is the pedestal. Therefore you need to make an S curve again.
		    setDelayAndTestGroup (peakingTime);
		    double cPedestal2 = this->measureSCurve();
		    LOG (INFO) <<"BBBBBBBBB found max Pedestal value "<< cPedestal2 << " at time "<< peakingTime;
		    ftpvsped->SetBinContent(350,cPedestal2);
		    ftpvsped->Draw("e2");
		    fCanvas2->Update();
		    TPamplitudes.push_back(testPulseAmplitude);
		    pedestals.push_back(cPedestal2);
		    peakingTimes.push_back(peakingTime);
		    skipFindDelayForEveryAmplitude = true;
    	  }
    }

    for(int i = 0; i<TPamplitudes.size(); i++){
	LOG(INFO) << "peaking time, TP  amplitude, pedestal " << peakingTimes[i] << "," <<  TPamplitudes[i] << "," << pedestals[i] ;
  }

  //making a graph of the gain and fitting it to get the gain
  TGraphErrors *graph_gain = new TGraphErrors(TPamplitudes.size(),&TPamplitudes[0],&pedestals[0],0,0);
  TF1 *lin_gain_fit = new TF1("lin_gain_fit", "pol1",100,1  );
  lin_gain_fit->SetLineColor(kRed);
  TCanvas *c_gain = new TCanvas("c_gain", "Gain fit");
  c_gain->SetFillColor(42);
  c_gain->SetGrid();
  graph_gain->Fit(lin_gain_fit);
  graph_gain->Draw();
  lin_gain_fit->Draw();
  graph_gain->Write();
  Double_t gain = lin_gain_fit->GetParameter(1);
  LOG(INFO) << "fitted gain: " << gain << " TP_LSB/Th_LSB";
  
  //convert the test pulse amplitude from LSB to fC
  Double_t TP_charge_step = 0.086/1.60218*10000; //charge step in e of the test pulse injection
  for(int i = 0; i < TPamplitudes.size(); i++){
  	TPamplitudesfC.push_back( TPamplitudes[i] * TP_charge_step);
  }
  TGraphErrors *graph_gainfC = new TGraphErrors(TPamplitudesfC.size(),&TPamplitudesfC[0],&pedestals[0],0,0);
  TCanvas *c_gainfC = new TCanvas("c_gainfC", "Gain fit fC");
  c_gain->SetFillColor(42);
  c_gain->SetGrid();
  graph_gainfC->Fit(lin_gain_fit);
  graph_gainfC->Draw();
  lin_gain_fit->Draw();
  graph_gainfC->Write();
  Double_t gainfC = lin_gain_fit->GetParameter(1);
  LOG(INFO) << "Converted fitted gain: " << 1./gainfC << " e/th";
 
    TDirectory* cDir = dynamic_cast< TDirectory* > ( gROOT->FindObject ("aaa" ) );

    if ( !cDir ) cDir = fResultFile->mkdir ( "aaa" );

    fResultFile->cd ( "aaa" );

    ftpvsped->Write ( ftpvsped->GetName(), TObject::kOverwrite );
    graph_gain->Write ( graph_gain->GetName(), TObject::kOverwrite );
    graph_gainfC->Write ( graph_gainfC->GetName(), TObject::kOverwrite );
    fCanvas2->Write (fCanvas2->GetName(), TObject::kOverwrite );
    c_gain->Write (c_gain->GetName(), TObject::kOverwrite );
    c_gainfC->Write (c_gainfC->GetName(), TObject::kOverwrite );
    fPulse->Write(fPulse->GetName(), TObject::kOverwrite );

    fResultFile->cd();
    fResultFile->Flush();

 
}

double mytool::measureSCurve()
{
    fChannel->resetHist();

    //loop the threshold
    for (uint8_t cVcth = 0; cVcth < 254; cVcth += 1)
    {
        //set the threshold
        this->fCbcInterface->WriteCbcReg (fCbc, "VCth", cVcth);

        //now read 20 Events
        this->ReadNEvents (fBoard, fNevents);
        const std::vector<Event*> cEvents = this->GetEvents (fBoard);

        //now loop the events and fill the histogram
        uint32_t cHitCounter = 0;

        for (auto cEvent : cEvents)
        {
            //if our channel fChan is hit, fill the histogram for channel
            if (cEvent->DataBit (fCbc->getFeId(), fCbc->getCbcId(), fChan) )
            {
                cHitCounter++;
                fChannel->fillHist (cVcth);
            }
        }

        //if (cHitCounter == fNevents) break;

    //    LOG (INFO) << "AAAAAAAAAA Measured " << cHitCounter << " hits in " << fNevents << " Events for VCth " << +cVcth;
    }
    //ok, now here we have a complete SCurve histogram from 0 to 255
    //differentiate it and return the pedestal
    //fCanvas->cd ();
    //fChannel->fScurve->Draw ("PX0");
    //TFile * bugfix = new TFile("TestepulseShape.root");
    fChannel->differentiateHist (fNevents, fHoleMode, 0, "VCth",fResultFile);
    double cPedestal = fChannel->getPedestal();
    LOG (INFO) << "Found SCurve midpoint at " << cPedestal;
    return cPedestal;
}

void mytool::setDelayAndTestGroup ( uint32_t pDelayns )
{
    //this is a little helper function to vary the Test pulse delay
    //set the fine delay on the CBC (cbc tp delay)
    //set the coarse delay on the D19C

    uint8_t cCoarseDelay = floor ( pDelayns  / 25 );
    uint8_t cFineDelay = ( cCoarseDelay * 25 ) + 24 - pDelayns;

    LOG (INFO) << "cFineDelay: " << +cFineDelay ;
    LOG (INFO) << "cCoarseDelay: " << +cCoarseDelay ;
    LOG (INFO) << "Current Time: " << +pDelayns ;

    //the name of the register controlling the TP timing on D19C
    std::string cTPDelayRegName = "fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse";

    fBeBoardInterface->WriteBoardReg (fBoard, cTPDelayRegName, cCoarseDelay);

    CbcRegWriter cWriter ( fCbcInterface, "SelTestPulseDel&ChanGroup", to_reg ( cFineDelay, fTestGroup ) );
    this->accept ( cWriter );
}

void mytool::parseSettings()
{
    //parse the settings
    auto cSetting = fSettingsMap.find ( "HoleMode" );

    if ( cSetting != std::end ( fSettingsMap ) )  fHoleMode = cSetting->second;
    else fHoleMode = 1;

    cSetting = fSettingsMap.find ( "Nevents" );

    if ( cSetting != std::end ( fSettingsMap ) ) fNevents = cSetting->second;
    else fNevents = 20;

    cSetting = fSettingsMap.find ( "TestPulsePotentiometer" );

    if ( cSetting != std::end ( fSettingsMap ) ) fTPAmplitude = cSetting->second;
    else fTPAmplitude = (fHoleMode) ? 50 : 200;
}
