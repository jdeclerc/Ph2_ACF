#ifndef _MYTOOL_H__
#define _MYTOOL_H__

#include "Tool.h"
#include "../Utils/Visitor.h"
#include "../Utils/CommonVisitors.h"
#include "Channel.h"
#include <TGraphErrors.h>

#include "TCanvas.h"
#include <TF1.h>
#include "TString.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

class mytool : public Tool
{
  public:
    mytool (uint8_t pChannel, uint8_t pTestGroup, int CST, int TMIN, int TMAX);
    ~mytool() {}

    void Initialize();
    double measureSCurve ();
    void SweepTPDelay();

  private:
    void setDelayAndTestGroup ( uint32_t pDelayns );
    void parseSettings();

    //from the settings map
    bool fHoleMode;
    uint32_t fNevents;
    uint8_t fTPAmplitude;

    //for the setup
    uint8_t fChan;
    uint8_t fTestGroup;
    int cst;
    int tmin;
    int tmax;
    //class to hold histogram for SCurve
    Channel* fChannel;

    //for our convenience
    Cbc* fCbc;
    BeBoard* fBoard;

    //root stuff
    TCanvas* fCanvas;
    TH1F* fPulse;
    TCanvas* fCanvas2;
    TH1F* ftpvsped;


};

#endif
